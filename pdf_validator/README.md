# Validate PDF content

Add some PDF files to `./data` first.

To test pdf validation run:

```sh
dart run
```

To run benchmarks:

```sh
dart run benchmark/benchmarks.dart
```
