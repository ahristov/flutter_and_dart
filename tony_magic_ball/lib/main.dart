import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(BallApp());
}

class BallApp extends StatelessWidget {
  const BallApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.blue,
        appBar: AppBar(
          title: Text('Ask me anything'),
          backgroundColor: Colors.blue.shade900,
        ),
        body: Ball(),
      ),
    );
  }
}

class Ball extends StatefulWidget {
  const Ball({Key? key}) : super(key: key);

  @override
  _BallState createState() => _BallState();
}

class _BallState extends State<Ball> {
  int ballNumber = 0;
  int getBallNumber() => Random().nextInt(4) + 1;
  void setBallNumber() {
    setState(() {
      ballNumber = getBallNumber();
    });
  }

  @override
  Widget build(BuildContext context) {
    setBallNumber();
    return Center(
      child: TextButton(
        child: Image.asset('images/ball$ballNumber.png'),
        onPressed: setBallNumber,
      ),
    );
  }
}
