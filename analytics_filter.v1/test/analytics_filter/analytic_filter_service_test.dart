import 'package:analytics_filter/analytic_event/analytic_event.dart';
import 'package:analytics_filter/analytic_filter/analytic_filter_data.dart';
import 'package:analytics_filter/analytic_filter/analytic_filter_element.dart';
import 'package:analytics_filter/analytic_filter/analytic_filter_service.dart';
import 'package:test/test.dart';

const _kMalformedConfigFile = 'test/analytics_filter/assets/analytics_filter_malformed.json';
const _kEmptyConfigFilePath = 'test/analytics_filter/assets/analytics_filter_empty.json';
const _kNoRulesConfigFile = 'test/analytics_filter/assets/analytics_filter_no_rules.json';
const _kEmptyRulesConfigFile = 'test/analytics_filter/assets/analytics_filter_empty_rules.json';
const _kAccountsConfigFile = 'test/analytics_filter/assets/analytics_filter_accounts.json';
const _kRdcConfigFile = 'test/analytics_filter/assets/analytics_filter_rdc.json';
const _kAccountsAndRdcConfigFile = 'test/analytics_filter/assets/analytics_filter_accounts_and_rdc.json';

const _kRemoteDeposit = 'remote_deposit';
const _kAccounts = 'accounts';
const _kAction = 'action';
const _kActivityShown = 'activity_shown';

final _eventDataRdcActivityShown = const AnalyticEvent(
  eventName: _kRemoteDeposit,
  eventParameters: {
    'n1': 'v1',
    _kAction: _kActivityShown,
    'n2': 'v2',
  },
);

final _eventDataAccountsActivityShown = const AnalyticEvent(
  eventName: _kAccounts,
  eventParameters: {
    'n1': 'v1',
    _kAction: _kActivityShown,
    'n2': 'v2',
  },
);

Future _assertWithConfigDataList<T>(T expected, AnalyticEvent event, List<AnalyticFilterData> configs) async {
  for (final config in configs) {
    await _assertWithConfigData(expected, event, config);
  }
}

Future _assertWithConfigData<T>(T expected, AnalyticEvent event, AnalyticFilterData configData) async {
  final filter = AnalyticFilterService.withConfigData(configData);
  final actual = await filter.applies(event);
  expect(actual, expected);
}

Future _assertWithConfigFile<T>(T expected, AnalyticEvent event, String configFilePath) async {
  final filter = AnalyticFilterService.withConfigFilePath(configFilePath);
  final actual = await filter.applies(event);
  expect(actual, expected);
}

Future main() async {
  group('Init with config file', () {
    test('Malformed config file, throws FormatException', () {
      final filter = AnalyticFilterService.withConfigFilePath(_kMalformedConfigFile);
      expect(() async => await filter.applies(_eventDataRdcActivityShown), throwsFormatException);
    });

    test('Missing config file, returns true',
        () async => await _assertWithConfigFile(true, _eventDataRdcActivityShown, 'file-does-bit-exist'));

    test('Empty config file, returns true',
        () async => await _assertWithConfigFile(true, _eventDataRdcActivityShown, _kEmptyConfigFilePath));

    test('Config file with no rules, returns true',
        () async => await _assertWithConfigFile(true, _eventDataRdcActivityShown, _kNoRulesConfigFile));

    test('Config file with empty rules, returns true',
        () async => await _assertWithConfigFile(true, _eventDataRdcActivityShown, _kEmptyRulesConfigFile));

    test('Accounts config skips RDC events',
        () async => await _assertWithConfigFile(false, _eventDataRdcActivityShown, _kAccountsConfigFile));

    test('RDC config logs RDC events',
        () async => await _assertWithConfigFile(true, _eventDataRdcActivityShown, _kRdcConfigFile));

    test('Accounts and RDC config logs Accounts events',
        () async => await _assertWithConfigFile(true, _eventDataAccountsActivityShown, _kAccountsAndRdcConfigFile));

    test('Accounts and RDC config logs RDC events',
        () async => await _assertWithConfigFile(true, _eventDataRdcActivityShown, _kAccountsAndRdcConfigFile));
  });
  group('Init with config data', () {
    test('No filters, returns true',
        () async => await _assertWithConfigData(true, _eventDataRdcActivityShown, AnalyticFilterData()));

    test('Empty filters, returns true',
        () async => await _assertWithConfigData(true, _eventDataRdcActivityShown, AnalyticFilterData(filters: [])));

    test('No name and no params, returns true', () async {
      final testConfigs = [
        AnalyticFilterData(
          filters: [
            AnalyticFilterElement(),
            AnalyticFilterElement(applicableName: ''),
            AnalyticFilterElement(applicableParameters: []),
            AnalyticFilterElement(applicableName: '', applicableParameters: []),
          ],
        ),
      ];
      _assertWithConfigDataList(true, _eventDataRdcActivityShown, testConfigs);
    });

    test('Other name and no params, returns false', () async {
      final testConfigs = [
        AnalyticFilterData(
          filters: [
            AnalyticFilterElement(applicableName: 'a'),
            AnalyticFilterElement(applicableName: 'b'),
            AnalyticFilterElement(applicableName: 'c'),
          ],
        ),
      ];
      _assertWithConfigDataList(false, _eventDataRdcActivityShown, testConfigs);
    });

    test('No name and other params, returns false', () async {
      final testConfigs = [
        AnalyticFilterData(
          filters: [
            AnalyticFilterElement(applicableParameters: ['a']),
            AnalyticFilterElement(applicableParameters: ['a', 'b', 'c']),
            AnalyticFilterElement(applicableParameters: ['a', '']),
          ],
        ),
      ];
      _assertWithConfigDataList(false, _eventDataRdcActivityShown, testConfigs);
    });

    test('Same name and no params, returns true', () async {
      final testConfigs = [
        AnalyticFilterData(
          filters: [
            AnalyticFilterElement(applicableName: _eventDataRdcActivityShown.eventName),
            AnalyticFilterElement(applicableName: _eventDataRdcActivityShown.eventName, applicableParameters: []),
            AnalyticFilterElement(applicableName: _eventDataRdcActivityShown.eventName, applicableParameters: ['']),
          ],
        ),
      ];
      _assertWithConfigDataList(true, _eventDataRdcActivityShown, testConfigs);
    });

    test('No name and same param, returns true', () async {
      final testConfigs = [
        AnalyticFilterData(
          filters: [
            AnalyticFilterElement(applicableParameters: ['', 'n1', _kAction, 'n2']),
          ],
        ),
      ];
      _assertWithConfigDataList(true, _eventDataRdcActivityShown, testConfigs);
    });
  });
}
