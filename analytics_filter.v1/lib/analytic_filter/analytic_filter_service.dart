import 'dart:convert';
import 'dart:io';

import 'package:analytics_filter/analytic_event/analytic_event.dart';
import 'package:analytics_filter/analytic_filter/analytic_filter_data.dart';

abstract class AnalyticFilterServiceBase {
  Future<bool> applies(AnalyticEvent event);
}

//

const String _kDefaultConfigFilePath = 'brandedAssets/configuration/empty_analytics_filter_config.json';

class AnalyticFilterService extends AnalyticFilterServiceBase {
  late final AnalyticFilterData configData;
  late final Future _initialized;

  /// Create instance and load data from the default JSON config file [configFilePath].
  AnalyticFilterService() : this.withConfigFilePath(_kDefaultConfigFilePath);

  /// Create instance and load data from the specified JSON config file [configFilePath].
  AnalyticFilterService.withConfigFilePath(String configFilePath) {
    _initialized = _init(configFilePath);
  }

  /// Create instance and initialize with the specified data [configData].
  AnalyticFilterService.withConfigData(this.configData) {
    _initialized = Future.value();
  }

  @override
  Future<bool> applies(AnalyticEvent event) async {
    await _initialized;

    // When no filters are specified, returns true
    if ((configData.filters ?? []).isEmpty) {
      return true;
    }

    final eventName = event.eventName;
    final eventParams = (event.eventParameters ?? {}).keys;

    for (final filter in configData.filters!) {
      final filterName = filter.applicableName ?? '';
      final filterParams = filter.applicableParameters ?? [];
      final nameApplies = filterName.isEmpty || filterName == eventName;
      final paramsApply = filterParams.isEmpty || filterParams.any((x) => eventParams.contains(x));

      if (nameApplies && paramsApply) {
        return true;
      }
    }

    return false;
  }

  Future _init(String configFilePath) async {
    configData = await _loadConfigData(configFilePath);
  }

  Future<String> _loadFileAsString(String fileName) async =>
      await File(fileName).exists() ? await File(fileName).readAsString() : '';

  Future<AnalyticFilterData> _loadConfigData(String fileName) async {
    final jsonString = await _loadFileAsString(fileName);
    if (jsonString.isEmpty) {
      return AnalyticFilterData();
    }

    final Map<String, dynamic> jsonContent = json.decode(jsonString) as Map<String, dynamic>;
    return AnalyticFilterData.fromJson(jsonContent);
  }
}
