import 'package:json_annotation/json_annotation.dart';

part 'analytic_filter_element.g.dart';

@JsonSerializable()
class AnalyticFilterElement {
  // If specified and not empty, filters event that have this name
  final String? applicableName;

  // If specified and not empty, filters event that has all at least one of these parameters
  final List<String>? applicableParameters;

  AnalyticFilterElement({this.applicableName, this.applicableParameters = const []});

  factory AnalyticFilterElement.fromJson(Map<String, dynamic> json) => _$AnalyticFilterElementFromJson(json);

  Map<String, dynamic> toJson() => _$AnalyticFilterElementToJson(this);
}
