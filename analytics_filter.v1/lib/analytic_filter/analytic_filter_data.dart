import 'package:analytics_filter/analytic_filter/analytic_filter_element.dart';
import 'package:json_annotation/json_annotation.dart';

part 'analytic_filter_data.g.dart';

@JsonSerializable()
class AnalyticFilterData {
  final List<AnalyticFilterElement>? filters;

  AnalyticFilterData({this.filters = const []});

  factory AnalyticFilterData.fromJson(Map<String, dynamic> json) => _$AnalyticFilterDataFromJson(json);

  Map<String, dynamic> toJson() => _$AnalyticFilterDataToJson(this);
}
