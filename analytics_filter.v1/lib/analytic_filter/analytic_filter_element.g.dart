// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'analytic_filter_element.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AnalyticFilterElement _$AnalyticFilterElementFromJson(
        Map<String, dynamic> json) =>
    AnalyticFilterElement(
      applicableName: json['applicableName'] as String?,
      applicableParameters: (json['applicableParameters'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
    );

Map<String, dynamic> _$AnalyticFilterElementToJson(
        AnalyticFilterElement instance) =>
    <String, dynamic>{
      'applicableName': instance.applicableName,
      'applicableParameters': instance.applicableParameters,
    };
