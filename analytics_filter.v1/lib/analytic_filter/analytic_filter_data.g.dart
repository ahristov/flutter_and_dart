// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'analytic_filter_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AnalyticFilterData _$AnalyticFilterDataFromJson(Map<String, dynamic> json) =>
    AnalyticFilterData(
      filters: (json['filters'] as List<dynamic>?)
              ?.map((e) =>
                  AnalyticFilterElement.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
    );

Map<String, dynamic> _$AnalyticFilterDataToJson(AnalyticFilterData instance) =>
    <String, dynamic>{
      'filters': instance.filters,
    };
