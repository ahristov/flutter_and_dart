import 'package:analytics_filter/analytic_event/collection_extentions.dart';

class AnalyticProviders {
  static const firebase = 1;
  static const apptentive = 2;
  static const adobeAnalytics = 4;

  final List<int> providers;

  /// Returns the bitmask of the providers passed in
  int get providersBitmask => providers.sum;

  const AnalyticProviders({required this.providers});
}
