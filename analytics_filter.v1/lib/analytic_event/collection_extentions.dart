/// Extensions that apply to iterables of numbers.
extension IterableNumberExtension on Iterable<int> {
  /// The sum of the elements.
  ///
  /// The sum is zero if the iterable is empty.
  int get sum {
    var result = 0;
    for (var value in this) {
      result += value;
    }
    return result;
  }
}
