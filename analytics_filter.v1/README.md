# Analytics filter v1

Project created with:

```sh
dart create -t console-simple --force .
```

To generate json serializable run:

```sh
dart run build_runner build
```

To run unit tests run:

```sh
dart test
# or #
dart test -r expanded
```

