import 'package:analytics_filter/analytic_event/analytic_event.dart';
import 'package:analytics_filter/analytic_event/analytic_providers.dart';

abstract class AnalyticsServiceBase {
  void eventHandler(AnalyticEvent event);
}

class AnalyticsService extends AnalyticsServiceBase {
  @override
  void eventHandler(AnalyticEvent event) {
    if (!_matchProvider(event: event)) {
      return;
    }

    if (!_matchFilter(event: event)) {
      return;
    }

    _logEvent(event: event);
  }

  bool _matchProvider({required AnalyticEvent event}) =>
      event.eventProviders.providersBitmask & AnalyticProviders.adobeAnalytics == AnalyticProviders.adobeAnalytics;

  bool _matchFilter({required AnalyticEvent event}) => true;

  Future<void> _logEvent({required AnalyticEvent event}) async => print('Log event $event to Adobe ...');
}
