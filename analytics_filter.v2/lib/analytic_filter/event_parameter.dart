import 'package:json_annotation/json_annotation.dart';

part 'event_parameter.g.dart';

@JsonSerializable()
class EventParameter {
  final String? parameterName;
  final List<String>? parameterValues;

  EventParameter({this.parameterName, this.parameterValues = const []});

  factory EventParameter.fromJson(Map<String, dynamic> json) => _$EventParameterFromJson(json);

  Map<String, dynamic> toJson() => _$EventParameterToJson(this);
}
