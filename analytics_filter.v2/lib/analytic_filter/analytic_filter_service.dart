import 'dart:convert';
import 'dart:io';

import 'package:analytics_filter/analytic_event/analytic_event.dart';
import 'package:analytics_filter/analytic_filter/analytic_filter_rule.dart';

import 'analytic_filter_config.dart';
import 'event_parameter.dart';

abstract class AnalyticFilterServiceBase {
  Future<bool> shouldInclude(AnalyticEvent event);
}

const String _kDefaultConfigFilePath = 'brandedAssets/configuration/empty_analytics_filter_config.json';

class AnalyticFilterService extends AnalyticFilterServiceBase {
  late final AnalyticFilterConfig filterConfig;
  late final Future _initialized;

  /// Create instance and load data from the default JSON config file [configFilePath].
  AnalyticFilterService() : this.withConfigFilePath(_kDefaultConfigFilePath);

  /// Create instance and load data from the specified JSON config file [configFilePath].
  AnalyticFilterService.withConfigFilePath(String configFilePath) {
    _initialized = _init(configFilePath);
  }

  /// Create instance and initialize with the specified data [filterConfig].
  AnalyticFilterService.withConfigData(this.filterConfig) {
    _initialized = Future.value();
  }

  @override
  Future<bool> shouldInclude(AnalyticEvent event) async {
    await _initialized;

    final denyRulesApply = (filterConfig.denyRules ?? []).isNotEmpty && _rulesApply(event, filterConfig.denyRules!);
    if (denyRulesApply) {
      return false;
    }

    final allowRulesApply = (filterConfig.allowRules ?? []).isEmpty || _rulesApply(event, filterConfig.allowRules!);
    return allowRulesApply;
  }

  bool _rulesApply(AnalyticEvent event, List<AnalyticFilterRule> rules) {
    final eventName = event.eventName.toLowerCase();

    for (final rule in rules) {
      final ruleEventName = (rule.eventName ?? '').toLowerCase();
      if (ruleEventName.isEmpty) {
        continue;
      }
      if (ruleEventName == eventName) {
        final ruleApplies = _eventParamsApply(event.eventParameters ?? {}, rule.eventParameters ?? []);
        if (ruleApplies) {
          return true;
        }
      }
    }

    return false;
  }

  bool _eventParamsApply(Map<String, String> eventParams, List<EventParameter> ruleEventParams) {
    if (ruleEventParams.isEmpty) {
      return true;
    }

    if (eventParams.isEmpty) {
      return false;
    }

    for (final ruleEventParam in ruleEventParams) {
      final ruleParamName = (ruleEventParam.parameterName ?? '').toLowerCase();
      final eventParam = eventParams[ruleParamName];
      if (eventParam == null) {
        continue;
      }

      final ruleParamValues = ruleEventParam.parameterValues ?? [];
      if (ruleParamValues.isEmpty) {
        return true;
      }

      if (ruleParamValues.contains(eventParam)) {
        return true;
      }
    }

    return false;
  }

  Future _init(String configFilePath) async {
    filterConfig = await _loadConfigData(configFilePath);
  }

  Future<String> _loadFileAsString(String filePath) async =>
      await File(filePath).exists() ? await File(filePath).readAsString() : '';

  Future<AnalyticFilterConfig> _loadConfigData(String filePath) async {
    final jsonString = await _loadFileAsString(filePath);
    if (jsonString.isEmpty) {
      return AnalyticFilterConfig();
    }

    final Map<String, dynamic> jsonContent = json.decode(jsonString) as Map<String, dynamic>;
    return AnalyticFilterConfig.fromJson(jsonContent);
  }
}
