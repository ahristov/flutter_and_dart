import 'package:analytics_filter/analytic_filter/analytic_filter_rule.dart';
import 'package:json_annotation/json_annotation.dart';

part 'analytic_filter_config.g.dart';

@JsonSerializable()
class AnalyticFilterConfig {
  final List<AnalyticFilterRule>? filterRules;

  AnalyticFilterConfig({this.filterRules = const []});

  factory AnalyticFilterConfig.fromJson(Map<String, dynamic> json) => _$AnalyticFilterConfigFromJson(json);

  Map<String, dynamic> toJson() => _$AnalyticFilterConfigToJson(this);
}
