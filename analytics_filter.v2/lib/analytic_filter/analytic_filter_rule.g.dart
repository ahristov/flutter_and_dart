// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'analytic_filter_rule.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AnalyticFilterRule _$AnalyticFilterRuleFromJson(Map<String, dynamic> json) =>
    AnalyticFilterRule(
      eventName: json['eventName'] as String?,
      eventParameters: (json['eventParameters'] as List<dynamic>?)
              ?.map((e) => EventParameter.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
    );

Map<String, dynamic> _$AnalyticFilterRuleToJson(AnalyticFilterRule instance) =>
    <String, dynamic>{
      'eventName': instance.eventName,
      'eventParameters': instance.eventParameters,
    };
