import 'package:json_annotation/json_annotation.dart';

import 'event_parameter.dart';

part 'analytic_filter_rule.g.dart';

@JsonSerializable()
class AnalyticFilterRule {
  final String? eventName;
  final List<EventParameter>? eventParameters;

  AnalyticFilterRule({this.eventName, this.eventParameters = const []});

  factory AnalyticFilterRule.fromJson(Map<String, dynamic> json) => _$AnalyticFilterRuleFromJson(json);

  Map<String, dynamic> toJson() => _$AnalyticFilterRuleToJson(this);
}
