// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_parameter.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventParameter _$EventParameterFromJson(Map<String, dynamic> json) =>
    EventParameter(
      parameterName: json['parameterName'] as String?,
      parameterValues: (json['parameterValues'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
    );

Map<String, dynamic> _$EventParameterToJson(EventParameter instance) =>
    <String, dynamic>{
      'parameterName': instance.parameterName,
      'parameterValues': instance.parameterValues,
    };
