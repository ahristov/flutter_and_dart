// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'analytic_filter_config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AnalyticFilterConfig _$AnalyticFilterConfigFromJson(
        Map<String, dynamic> json) =>
    AnalyticFilterConfig(
      denyRules: (json['denyRules'] as List<dynamic>?)
              ?.map(
                  (e) => AnalyticFilterRule.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
      allowRules: (json['allowRules'] as List<dynamic>?)
              ?.map(
                  (e) => AnalyticFilterRule.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
    );

Map<String, dynamic> _$AnalyticFilterConfigToJson(
        AnalyticFilterConfig instance) =>
    <String, dynamic>{
      'denyRules': instance.denyRules,
      'allowRules': instance.allowRules,
    };
