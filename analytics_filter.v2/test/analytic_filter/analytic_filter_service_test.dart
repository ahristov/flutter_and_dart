import 'package:analytics_filter/analytic_event/analytic_event.dart';
import 'package:analytics_filter/analytic_filter/analytic_filter_config.dart';
import 'package:analytics_filter/analytic_filter/analytic_filter_rule.dart';
import 'package:analytics_filter/analytic_filter/analytic_filter_service.dart';
import 'package:analytics_filter/analytic_filter/event_parameter.dart';
import 'package:test/test.dart';

const _kMalformedConfigFile = 'test/analytic_filter/assets/analytic_filter_malformed.json';
const _kEmptyConfigFilePath = 'test/analytic_filter/assets/analytic_filter_empty.json';
const _kNoRulesConfigFile = 'test/analytic_filter/assets/analytic_filter_no_rules.json';
const _kEmptyRulesConfigFile = 'test/analytic_filter/assets/analytic_filter_empty_rules.json';
const _kAccountsConfigFile = 'test/analytic_filter/assets/analytic_filter_accounts.json';
const _kAccountActionsConfigFile = 'test/analytic_filter/assets/analytic_filter_account_actions.json';

const _kRemoteDeposit = 'remote_deposit';
const _kAccounts = 'accounts';
const _kAccountActions = 'account_actions';
const _kAction = 'action';
const _kActivityShown = 'activity_shown';
const _kSelected = 'selected';
const _kTransfers = 'transfers';
const _kBillPay = 'bill_pay';
const _kPayNow = 'pay_now';

final _eventDataRdcActivityShown = const AnalyticEvent(
  eventName: _kRemoteDeposit,
  eventParameters: {
    'n1': 'v1',
    _kAction: _kActivityShown,
    'n2': 'v2',
  },
);

final _eventDataAccountSelected = const AnalyticEvent(
  eventName: _kAccounts,
  eventParameters: {_kAction: _kSelected},
);

final _eventDataAcctActionTransfers = const AnalyticEvent(
  eventName: _kAccountActions,
  eventParameters: {_kAction: _kTransfers},
);

final _eventDataAcctActionRdc = const AnalyticEvent(
  eventName: _kAccountActions,
  eventParameters: {_kAction: _kRemoteDeposit},
);

final _eventDataAcctActionBillPay = const AnalyticEvent(
  eventName: _kAccountActions,
  eventParameters: {_kAction: _kBillPay},
);

final _eventDataAcctActionPayNow = const AnalyticEvent(
  eventName: _kAccountActions,
  eventParameters: {_kAction: _kPayNow},
);

Future _assertWithConfigDataList<T>(T expected, AnalyticEvent event, List<AnalyticFilterConfig> configs) async {
  for (final config in configs) {
    await _assertWithConfigData(expected, event, config);
  }
}

Future _assertWithConfigData<T>(T expected, AnalyticEvent event, AnalyticFilterConfig configData) async {
  final filter = AnalyticFilterService.withConfigData(configData);
  final actual = await filter.shouldInclude(event);
  expect(actual, expected);
}

Future _assertWithConfigFile<T>(T expected, AnalyticEvent event, String configFilePath) async {
  final filter = AnalyticFilterService.withConfigFilePath(configFilePath);
  final actual = await filter.shouldInclude(event);
  expect(actual, expected);
}

Future main() async {
  group('Init with config file:', () {
    test('Malformed config file, throws FormatException', () {
      final filter = AnalyticFilterService.withConfigFilePath(_kMalformedConfigFile);
      expect(() async => await filter.shouldInclude(_eventDataRdcActivityShown), throwsFormatException);
    });
    test('Missing config file, returns true',
        () async => await _assertWithConfigFile(true, _eventDataRdcActivityShown, 'file-does-bit-exist'));

    test('Empty config file should include RDC event',
        () async => await _assertWithConfigFile(true, _eventDataRdcActivityShown, _kEmptyConfigFilePath));

    test('Config file with no rules should include RDC event',
        () async => await _assertWithConfigFile(true, _eventDataRdcActivityShown, _kNoRulesConfigFile));

    test('Config file with empty rules should include RDC event',
        () async => await _assertWithConfigFile(true, _eventDataRdcActivityShown, _kEmptyRulesConfigFile));

    test('Config file with accounts should include accounts selected',
        () async => await _assertWithConfigFile(true, _eventDataAccountSelected, _kAccountsConfigFile));

    test('Config file with account actions should include transfers',
        () async => await _assertWithConfigFile(true, _eventDataAcctActionTransfers, _kAccountActionsConfigFile));

    test('Config file with account actions should include remote deposit',
        () async => await _assertWithConfigFile(true, _eventDataAcctActionRdc, _kAccountActionsConfigFile));

    test('Config file with account actions should include pay now',
        () async => await _assertWithConfigFile(true, _eventDataAcctActionPayNow, _kAccountActionsConfigFile));

    test('Config file with account actions should exclude bill pay',
        () async => await _assertWithConfigFile(false, _eventDataAcctActionBillPay, _kAccountActionsConfigFile));
  });
  group('Init with config data:', () {
    test('No filters should include event',
        () async => await _assertWithConfigData(true, _eventDataRdcActivityShown, AnalyticFilterConfig()));
    test(
        'Empty filters should include event',
        () async => await _assertWithConfigData(
            true, _eventDataRdcActivityShown, AnalyticFilterConfig(denyRules: [], allowRules: [])));
    test(
        'Deny without eventName should include event',
        () async => await _assertWithConfigData(
            true, _eventDataRdcActivityShown, AnalyticFilterConfig(denyRules: [AnalyticFilterRule()], allowRules: [])));
    test(
        'Deny by eventName should exclude event',
        () async => await _assertWithConfigDataList(false, _eventDataRdcActivityShown, [
              AnalyticFilterConfig(
                denyRules: [
                  AnalyticFilterRule(eventName: _kRemoteDeposit),
                ],
                allowRules: [],
              ),
              AnalyticFilterConfig(
                denyRules: [
                  AnalyticFilterRule(eventName: 'x'),
                  AnalyticFilterRule(eventName: _kRemoteDeposit),
                ],
                allowRules: [],
              ),
              AnalyticFilterConfig(
                denyRules: [
                  AnalyticFilterRule(eventName: 'x'),
                  AnalyticFilterRule(eventName: _kRemoteDeposit),
                  AnalyticFilterRule(eventName: 'x'),
                ],
                allowRules: [],
              ),
              AnalyticFilterConfig(
                denyRules: [
                  AnalyticFilterRule(eventName: _kRemoteDeposit),
                ],
                allowRules: [
                  AnalyticFilterRule(eventName: _kRemoteDeposit),
                ],
              ),
              AnalyticFilterConfig(
                denyRules: [
                  AnalyticFilterRule(eventName: 'x'),
                  AnalyticFilterRule(eventName: _kRemoteDeposit),
                ],
                allowRules: [
                  AnalyticFilterRule(eventName: _kRemoteDeposit),
                ],
              ),
              AnalyticFilterConfig(
                denyRules: [
                  AnalyticFilterRule(eventName: 'x'),
                  AnalyticFilterRule(eventName: _kRemoteDeposit),
                  AnalyticFilterRule(eventName: 'x'),
                ],
                allowRules: [
                  AnalyticFilterRule(eventName: _kRemoteDeposit),
                ],
              ),
            ]));

    test(
        'Allow by eventName should include event',
        () async => await _assertWithConfigDataList(true, _eventDataRdcActivityShown, [
              AnalyticFilterConfig(allowRules: [
                AnalyticFilterRule(eventName: _kRemoteDeposit),
              ]),
              AnalyticFilterConfig(allowRules: [
                AnalyticFilterRule(eventName: 'x'),
                AnalyticFilterRule(eventName: _kRemoteDeposit),
              ]),
              AnalyticFilterConfig(allowRules: [
                AnalyticFilterRule(eventName: 'x'),
                AnalyticFilterRule(eventName: _kRemoteDeposit),
                AnalyticFilterRule(eventName: 'x'),
              ]),
              AnalyticFilterConfig(denyRules: [
                AnalyticFilterRule(eventName: 'x'),
              ], allowRules: [
                AnalyticFilterRule(eventName: _kRemoteDeposit),
              ]),
            ]));
    test(
        'Allow by eventName and empty eventParameter should include event',
        () async => await _assertWithConfigDataList(true, _eventDataRdcActivityShown, [
              AnalyticFilterConfig(allowRules: [
                AnalyticFilterRule(eventName: _kRemoteDeposit, eventParameters: []),
              ]),
            ]));
    test(
        'Allow by eventName and same eventParameter should include event',
        () async => await _assertWithConfigDataList(true, _eventDataRdcActivityShown, [
              AnalyticFilterConfig(allowRules: [
                AnalyticFilterRule(eventName: _kRemoteDeposit, eventParameters: [
                  EventParameter(parameterName: _kAction),
                ])
              ]),
              AnalyticFilterConfig(allowRules: [
                AnalyticFilterRule(eventName: _kRemoteDeposit, eventParameters: [
                  EventParameter(parameterName: 'x'),
                  EventParameter(parameterName: _kAction),
                ])
              ]),
              AnalyticFilterConfig(allowRules: [
                AnalyticFilterRule(eventName: _kRemoteDeposit, eventParameters: [
                  EventParameter(parameterName: 'x'),
                  EventParameter(parameterName: _kAction),
                  EventParameter(parameterName: 'x'),
                ])
              ]),
              AnalyticFilterConfig(allowRules: [
                AnalyticFilterRule(eventName: _kRemoteDeposit, eventParameters: [
                  EventParameter(
                    parameterName: _kAction,
                    parameterValues: ['x', _kActivityShown, 'x'],
                  ),
                ])
              ]),
              AnalyticFilterConfig(allowRules: [
                AnalyticFilterRule(eventName: _kRemoteDeposit, eventParameters: [
                  EventParameter(parameterName: 'x'),
                ]),
                AnalyticFilterRule(eventName: _kRemoteDeposit, eventParameters: [
                  EventParameter(
                    parameterName: _kAction,
                    parameterValues: ['x', _kActivityShown, 'x'],
                  ),
                ]),
              ]),
            ]));
    test(
        'Allow by eventName and other eventParameter should exclude event',
        () async => await _assertWithConfigDataList(false, _eventDataRdcActivityShown, [
              AnalyticFilterConfig(allowRules: [
                AnalyticFilterRule(eventName: _kRemoteDeposit, eventParameters: [
                  EventParameter(parameterName: 'x'),
                ]),
                AnalyticFilterRule(eventName: _kRemoteDeposit, eventParameters: [
                  EventParameter(parameterName: 'x'),
                  EventParameter(parameterName: _kAction, parameterValues: ['x', 'y', 'z']),
                ]),
                AnalyticFilterRule(eventName: _kRemoteDeposit, eventParameters: [
                  EventParameter(parameterName: 'x'),
                  EventParameter(parameterName: _kAction, parameterValues: ['x', 'y', 'z']),
                  EventParameter(parameterName: 'y'),
                  EventParameter(parameterName: 'z', parameterValues: ['a', 'b', 'c']),
                ]),
              ]),
            ]));
  });
}
