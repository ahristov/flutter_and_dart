# blog_web_app

Code and notes from [flutter-provider-state-management](https://www.udemy.com/course/flutter-provider-state-management)

The course demonstrates:

- Provider
- FutureProvider
- StreamProvider
- ValueNotifier
- ChangeNotifier
- ChangeNotifierProvider
- Extension
- Provider extension methods
  - context.watch()
  - context.read()
  - context.select()
- Consumer and selector
- Firebase Firestore
- Firebase Authentication

Installed Visual Studio extensions:

- Dart Data Class Generator
- Error Lens
- Pubspec Assist

## Section 2: Basic concepts

Provider: A wrapper around `InheritedWidget`.
InheritedWidget: A widget that propagates information down the widget tree.

The `Context` provides data like `Theme`, `Navigator`, etc. The provider adds custom data:

```dart
Provider(
  create: (context) => User(),
  child: MyApp(),
)
...
// MyApp widget
Widget build(BuildContext context) {
  final user = Provider.of<User>(context);
}
```

The data can be a simple type like `bool`, `String`, `int`, etc.
