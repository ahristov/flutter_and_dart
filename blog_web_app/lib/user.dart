class User {
  final String name;
  final String profilePicture;

  User({
    required this.name,
    required this.profilePicture,
  });
}
