import 'package:blog_web_app/blog_entry_page.dart';
import 'package:blog_web_app/blog_post.dart';
import 'package:blog_web_app/blog_scaffold.dart';
import 'package:blog_web_app/user.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'blog_page.dart';
import 'constants.dart';
import 'constrained_centre.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final posts = Provider.of<List<BlogPost>>(context);
    final user = Provider.of<User>(context);
    return BlogScaffold(
      children: [
        ConstrainedCentre(
          child: CircleAvatar(
            backgroundImage: NetworkImage(user.profilePicture),
            radius: 72,
          ),
        ),
        kRowSpaceMedium,
        ConstrainedCentre(
          child: SelectableText(
            user.name,
            style: Theme.of(context).textTheme.headline1,
          ),
        ),
        kRowSpaceLarge,
        SelectableText(
          'Et laborum ea minim eiusmod ad eiusmod sit. Nostrud dolore voluptate voluptate velit nostrud nostrud excepteur nostrud laboris sunt.',
          style: Theme.of(context).textTheme.bodyText2,
        ),
        kRowSpaceLarge,
        SelectableText(
          'Blog',
          style: Theme.of(context).textTheme.headline2,
        ),
        for (final post in posts) BlogListTile(blogPost: post),
      ],
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
            return BlogEntryPage();
          }));
        },
        label: Text('New Blog'),
        icon: Icon(Icons.add),
      ),
    );
  }
}

class BlogListTile extends StatelessWidget {
  final BlogPost blogPost;
  BlogListTile({Key? key, required this.blogPost}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        kRowSpaceMedium,
        InkWell(
          child: Text(
            blogPost.title,
            style: TextStyle(color: Colors.blueAccent.shade700),
          ),
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) {
                  return BlogPage(blogPost: blogPost);
                },
              ),
            );
          },
        ),
        kRowSpaceMedium,
        SelectableText(
          blogPost.date,
          style: Theme.of(context).textTheme.caption,
        )
      ],
    );
  }
}
