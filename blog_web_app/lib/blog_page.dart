import 'package:blog_web_app/blog_scaffold.dart';
import 'package:blog_web_app/user.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'blog_post.dart';
import 'constants.dart';
import 'constrained_centre.dart';

class BlogPage extends StatelessWidget {
  final BlogPost blogPost;

  const BlogPage({Key? key, required this.blogPost}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User>(context);

    return BlogScaffold(children: [
      ConstrainedCentre(
        child: CircleAvatar(
          backgroundImage: NetworkImage(user.profilePicture),
          radius: 54,
        ),
      ),
      kRowSpaceMedium,
      ConstrainedCentre(
        child: SelectableText(
          user.name,
          style: Theme.of(context).textTheme.headline5,
        ),
      ),
      kRowSpaceLarge,
      SelectableText(
        blogPost.title,
        style: Theme.of(context).textTheme.headline1,
      ),
      kRowSpaceMedium,
      SelectableText(
        blogPost.date,
        style: Theme.of(context).textTheme.caption,
      ),
      kRowSpaceLarge,
      SelectableText(
        blogPost.body,
        style: Theme.of(context).textTheme.caption,
      ),
    ]);
  }
}
