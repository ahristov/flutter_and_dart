import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';

class BlogPost {
  final String title;
  final DateTime publishedDate;
  final String body;

  String get date => DateFormat('d MMMM y').format(publishedDate);

  BlogPost({
    required this.title,
    required this.publishedDate,
    required this.body,
  });

  BlogPost copyWith({
    String? title,
    DateTime? publishedDate,
    String? body,
  }) {
    return BlogPost(
      title: title ?? this.title,
      publishedDate: publishedDate ?? this.publishedDate,
      body: body ?? this.body,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'published_date': Timestamp.fromDate(publishedDate),
      'body': body,
    };
  }

  factory BlogPost.fromMap(Map<String, dynamic> map) {
    return BlogPost(
      title: map['title'],
      publishedDate: map['published_date'].toDate(),
      body: map['body'],
    );
  }

  String toJson() => json.encode(toMap());

  factory BlogPost.fromJson(String source) =>
      BlogPost.fromMap(json.decode(source));

  @override
  String toString() =>
      'BlogPost(title: $title, publishedDate: $publishedDate, body: $body)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is BlogPost &&
        other.title == title &&
        other.publishedDate == publishedDate &&
        other.body == body;
  }

  @override
  int get hashCode => title.hashCode ^ publishedDate.hashCode ^ body.hashCode;
}
