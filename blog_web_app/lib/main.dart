import 'package:blog_web_app/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'blog_post.dart';
import 'home_page.dart';

var theme = ThemeData(
  primarySwatch: Colors.blue,
  textTheme: TextTheme(
    headline1: TextStyle(
      fontSize: 45,
      fontWeight: FontWeight.bold,
      color: Colors.black,
    ),
    headline2: TextStyle(
      fontSize: 27,
      fontWeight: FontWeight.bold,
      color: Colors.black,
    ),
    bodyText2: TextStyle(
      fontSize: 22,
      height: 1.4,
    ),
    caption: TextStyle(
      fontSize: 18,
      height: 1.4,
    ),
  ),
  appBarTheme: AppBarTheme(
    color: Colors.transparent,
    elevation: 0,
    iconTheme: IconThemeData(
      color: Colors.black,
    ),
  ),
);

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        StreamProvider<List<BlogPost>>(
          create: (context) => blogPostsStream(),
          initialData: [],
        ),
        // Provider<List<BlogPost>>(create: (context) => _blogPosts),
        Provider<User>(
          create: (context) => User(
            name: 'Flutter Dev',
            profilePicture: 'https://cdn130.picsart.com/315363064349201.jpg',
          ),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Dev Blog',
        theme: theme,
        home: HomePage(),
      ),
    );
  }
}

Future<List<BlogPost>> blogPosts() async {
  return await FirebaseFirestore.instance.collection('blogs').get().then(
      (query) =>
          query.docs.map((doc) => BlogPost.fromMap(doc.data())).toList());
}

Stream<List<BlogPost>> blogPostsStream() {
  return FirebaseFirestore.instance
      .collection('blogs')
      .snapshots()
      .map((snapshot) {
    return snapshot.docs.map((doc) => BlogPost.fromMap(doc.data())).toList()
      ..sort((first, last) {
        final firstDate = first.publishedDate;
        final lastDate = last.publishedDate;
        return -firstDate.compareTo(lastDate);
      });
  });
}
