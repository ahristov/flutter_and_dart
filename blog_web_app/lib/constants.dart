import 'package:flutter/material.dart';

const kRowSpaceSmall = SizedBox(height: 10);
const kRowSpaceMedium = SizedBox(height: 18);
const kRowSpaceLarge = SizedBox(height: 40);
const kHorizontalPadding = EdgeInsets.symmetric(horizontal: 18);
