import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:soundpool/soundpool.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: XylophonePage(),
        ),
      ),
    );
  }
}

class XylophonePage extends StatefulWidget {
  const XylophonePage({Key? key}) : super(key: key);

  @override
  _XylophonePageState createState() => _XylophonePageState();
}

class _XylophonePageState extends State<XylophonePage> {
  List<int> soundIds = [];

  Soundpool soundpool = Soundpool.fromOptions(
    options: SoundpoolOptions(
      streamType: StreamType.notification,
    ),
  );

  Future<void> initSounds() async {
    for (int i = 1; i <= 7; i++) {
      soundIds.add(await loadSound(i - 1));
    }
  }

  Future<int> loadSound(int id) async {
    var asset = await rootBundle.load("assets/note$id.wav");
    return await soundpool.load(asset);
  }

  Widget buildButton({int soundNumber = 1, Color? color = Colors.grey}) {
    return Expanded(
      child: TextButton(
        child: Text(''),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(color),
        ),
        onPressed: () async {
          await soundpool.play(soundIds[soundNumber - 1]);
        },
      ),
    );
  }

  @override
  void initState() {
    initSounds();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        buildButton(soundNumber: 1, color: Colors.red),
        buildButton(soundNumber: 2, color: Colors.orange),
        buildButton(soundNumber: 3, color: Colors.yellow),
        buildButton(soundNumber: 4, color: Colors.green),
        buildButton(soundNumber: 5, color: Colors.teal),
        buildButton(soundNumber: 6, color: Colors.blue),
        buildButton(soundNumber: 7, color: Colors.deepPurple),
      ],
    );
  }
}
