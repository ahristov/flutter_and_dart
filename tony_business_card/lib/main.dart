import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.deepPurple,
        body: SafeArea(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              CircleAvatar(
                radius: 70.0,
                backgroundImage: AssetImage('images/Tony_Hristov.png'),
              ),
              Text(
                'Tony Hristov',
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.deepPurple.shade100,
                  fontFamily: 'Bree Serif',
                  fontWeight: FontWeight.bold,
                ),
              ),
              Text(
                'DEVELOPER',
                style: TextStyle(
                    fontSize: 22,
                    color: Colors.deepPurple.shade200,
                    fontFamily: 'Source Sans Pro',
                    fontWeight: FontWeight.bold,
                    letterSpacing: 4.0),
              ),
              SizedBox(
                height: 30.0,
                width: 240.0,
                child: Divider(
                  color: Colors.deepPurple.shade300,
                ),
              ),
              Card(
                color: Colors.grey.shade400,
                margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: ListTile(
                  leading: Icon(
                    Icons.phone,
                    size: 26,
                    color: Colors.deepPurple,
                  ),
                  title: Text(
                    '+1 (469) 475-2752',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontFamily: 'Source Sans Pro',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              Card(
                color: Colors.grey.shade400,
                margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: ListTile(
                  leading: Icon(
                    Icons.email,
                    size: 26,
                    color: Colors.deepPurple,
                  ),
                  title: Text(
                    'atanashristov@hotmail.com',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                      fontFamily: 'Source Sans Pro',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
