import 'package:dart_helloworld/dart_helloworld.dart' as dart_helloworld;

void main(List<String> arguments) {
  const name = 'Atanas Hristov';
  print('Hello ${name}: ${dart_helloworld.calculate()}!');
}
