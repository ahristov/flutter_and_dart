import 'package:flutter/material.dart';

class ContentUIVerticalCards extends StatelessWidget {
  const ContentUIVerticalCards({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => build3(context);

  Widget build3(BuildContext context) {
    const st1 = TextStyle(color: Colors.grey, fontSize: 26, fontWeight: FontWeight.bold);
    const st2 = TextStyle(color: Colors.black, fontSize: 22, fontWeight: FontWeight.normal);
    const space = SizedBox(height: 30);

    final menuItems = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        space,
        Text('Menu 1', style: st1),
        Text('Text 1', style: st2),
        space,
        Text('Menu 2', style: st1),
        Text('Text 2', style: st2),
      ],
    );

    final showCards = true;

    return Stack(
      children: <Widget>[
        Column(
          children: [
            Text(
              'Header',
              style: TextStyle(fontSize: 42),
            ),
            Flexible(
              child: ListView(
                padding: EdgeInsets.symmetric(
                  vertical: 20,
                  horizontal: 30,
                ),
                shrinkWrap: true,
                children: [
                  SizedBox(height: 30),
                  menuItems,
                ],
              ),
            ),
          ],
        ),
        if (showCards)
          Positioned(
            left: MediaQuery.of(context).size.width - 100,
            top: MediaQuery.of(context).size.height / 6,
            // right: MediaQuery.of(context).size.width - 10,
            // bottom: MediaQuery.of(context).size.height / 4,
            child: Container(
              color: Colors.blue[300],
              child: Text('This is the vertical cards'),
              height: 200,
            ),
          ),
      ],
    );
  }

  Widget build2(BuildContext context) {
    return Container(
      color: Colors.grey[300],
      // height: MediaQuery.of(context).size.height / 2,
      // width: MediaQuery.of(context).size.width / 2,
      child: Stack(
        children: <Widget>[
          Positioned(
            left: MediaQuery.of(context).size.width - 100,
            top: MediaQuery.of(context).size.height / 6,
            // right: MediaQuery.of(context).size.width - 10,
            // bottom: MediaQuery.of(context).size.height / 4,
            child: Container(
              color: Colors.blue[300],
              child: Text('This is the vertical cards'),
              height: 200,
            ),
          ),
        ],
      ),
    );
  }

  Widget build1(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Positioned(
            // red box
            child: Container(
              child: Text(
                "Lorem ipsum",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              decoration: BoxDecoration(
                color: Colors.red[400],
              ),
              padding: EdgeInsets.all(16),
            ),
            left: 24,
            top: 24,
          ),
        ],
      ),
      // width: 320,
      // height: 240,
      color: Colors.grey[300],
    );
  }
}
