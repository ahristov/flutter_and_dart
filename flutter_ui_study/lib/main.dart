import 'package:flutter/material.dart';
import 'package:flutter_ui_study/playground/content_ui_interstitial.dart';
import 'package:flutter_ui_study/playground/content_ui_vertical_cards.dart';
import 'package:storybook_flutter/storybook_flutter.dart';

import 'playground/horizontal_scrollable_list_view.dart';
import 'flutter_widgets/flutter_widgets.dart';

void main() {
  runApp(StoryBookExample());
}

class StoryBookExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Storybook(
        children: [
          ...flutterBasicsStories,
          ...flutterLayoutStories,
          ...flutterMaterialStories,
          ..._playgroundStories,
          ..._demoStories,
        ],
      );
}

class CustomStoryBookExample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final decoration = BoxDecoration(
      border: Border(
        right: BorderSide(color: Theme.of(context).dividerColor),
        left: BorderSide(color: Theme.of(context).dividerColor),
      ),
      color: Theme.of(context).cardColor,
    );
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: CustomStorybook(
          builder: (context) => Row(
            children: [
              Container(
                width: 200,
                decoration: decoration,
                child: const Contents(),
              ),
              const Expanded(child: CurrentStory()),
              Container(
                width: 200,
                decoration: decoration,
                child: const KnobPanel(),
              ),
            ],
          ),
          children: [
            ...flutterBasicsStories,
            ...flutterLayoutStories,
            ...flutterMaterialStories,
            ..._playgroundStories,
            ..._demoStories,
          ],
        ),
      ),
    );
  }
}

const _kSectionPlayground = 'Playground';
final _playgroundStories = [
  Story.simple(
    name: 'Interstitial',
    section: _kSectionPlayground,
    child: ContentUIInterstitialApp(),
  ),
  Story.simple(
    name: 'Vertical cards',
    section: _kSectionPlayground,
    child: ContentUIVerticalCards(),
  ),
  Story.simple(
    name: 'Scrollable List View',
    section: _kSectionPlayground,
    child: HorizontalScrollableListViewApp(),
  )
];

const _kSectionUsage = '_usage';
final _demoStories = [
  Story.simple(
    name: 'Simple',
    section: _kSectionUsage,
    child: ElevatedButton(
      onPressed: () {},
      child: const Text('Push me'),
    ),
  ),
  Story(
    name: 'Knobs',
    section: _kSectionUsage,
    builder: (context, k) => ElevatedButton(
      onPressed: k.boolean(label: 'Enabled', initial: true) ? () {} : null,
      child: Text(k.text(label: 'Text', initial: 'Push me')),
    ),
  ),
  Story(
    name: 'Custom story',
    section: _kSectionUsage,
    padding: const EdgeInsets.all(8),
    background: Colors.red,
    builder: (context, k) => ElevatedButton(
      onPressed: k.boolean(label: 'Enabled', initial: true) ? () {} : null,
      child: Text(k.text(label: 'Text', initial: 'Push me')),
    ),
  ),
  Story(
    name: 'Wrapper widget',
    section: _kSectionUsage,
    wrapperBuilder: (context, story, child) => Container(
      decoration: BoxDecoration(border: Border.all()),
      margin: const EdgeInsets.all(16),
      child: Center(child: child),
    ),
    builder: (context, k) => ElevatedButton(
      onPressed: k.boolean(label: 'Enabled', initial: true) ? () {} : null,
      child: Text(k.text(label: 'Text', initial: 'Push me')),
    ),
  ),
];
