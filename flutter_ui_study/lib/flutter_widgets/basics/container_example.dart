import 'package:flutter/material.dart';

/*

## Container widget

Container widget:  helps decorate, compose and position child widgets.

Without anythign else, the container sizes to it's child. 

*/

class ContainerExample extends StatelessWidget {
  final Color? color;
  final EdgeInsetsGeometry? padding;
  final EdgeInsetsGeometry? margin;
  const ContainerExample({
    this.color,
    this.padding,
    this.margin,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black12,
      padding: EdgeInsets.all(20.0),
      child: Container(
        child: Text(
          'Text in container',
          style: Theme.of(context).textTheme.headline5,
        ),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black),
          color: color,
        ),
        padding: padding,
        margin: margin,
      ),
    );
  }
}
