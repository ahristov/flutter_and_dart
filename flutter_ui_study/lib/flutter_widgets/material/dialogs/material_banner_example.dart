/*

## MaterialBanner widget

MaterialBanner widget diplays message to the user to address (or dismiss the banner).
It is displayed at the top of the screen, bellow the top bar.

See: https://api.flutter.dev/flutter/material/MaterialBanner-class.html

*/

import 'dart:developer';

import 'package:expandable_text/expandable_text.dart';
import 'package:flutter/material.dart';

class MaterialBannerExample extends StatelessWidget {
  final Color? color;
  final Color? backgroundColor;
  final EdgeInsetsGeometry? padding;
  final bool dismissable;
  const MaterialBannerExample({
    this.color,
    this.backgroundColor,
    this.padding,
    this.dismissable = false,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialBanner(
      padding: padding,
      backgroundColor: backgroundColor,
      leadingPadding: EdgeInsetsDirectional.only(end: 18),
      leading: Container(
        child: IconButton(
          padding: EdgeInsets.zero,
          alignment: Alignment.topCenter,
          constraints: BoxConstraints(maxWidth: 30),
          iconSize: 18,
          icon: Icon(
            Icons.info_outline,
            color: color,
          ),
          onPressed: null,
        ),
      ),
      contentTextStyle: TextStyle(
        color: color,
        fontSize: 16,
        height: 24 / 16, // Line height = fontSize * height.
      ),
      content: ExpandableText(
        'Deserunt ad commodo do excepteur ad incididunt excepteur sit commodo. Proident eiusmod do aliqua consectetur laborum voluptate Lorem incididunt commodo minim.',
        expandText: '',
        collapseText: '',
        maxLines: 3,
        expandOnTextTap: true,
        collapseOnTextTap: true,
        linkColor: color,
        onExpandedChanged: (expanded) => {},
      ),
      actions: [
        if (!dismissable) SizedBox.shrink(),
        if (dismissable)
          IconButton(
            padding: EdgeInsets.zero,
            alignment: Alignment.topCenter,
            constraints: BoxConstraints(maxWidth: 30),
            iconSize: 18,
            icon: Icon(
              Icons.close,
              color: color,
            ),
            onPressed: () {
              log('MaterialBanner: dismiss pressed');
            },
          ),
      ],
    );
  }
}
