import 'package:flutter/material.dart';
import 'package:flutter_ui_study/flutter_widgets/common.dart';
import 'package:flutter_ui_study/flutter_widgets/layout/fitted_box_example.dart';
import 'package:storybook_flutter/storybook_flutter.dart';

import 'basics/container_example.dart';
import 'layout/wrap_example.dart';
import 'material/dialogs/material_banner_example.dart';

// See: [Material widgets catalog](https://flutter.dev/docs/development/ui/widgets)

const _kMaterialSectionName = 'Flutter-Material';
const _kMaterialLayout = 'Dialogs';
final flutterMaterialStories = [
  Story(
    name: '$_kMaterialLayout: MaterialBanner',
    section: _kMaterialSectionName,
    builder: (context, k) => MaterialBannerExample(
      color: k.options(
        label: 'color',
        initial: Common.color('white'),
        options: Common.colorsNull(),
      ),
      backgroundColor: k.options(
        label: 'bg-color',
        initial: Common.color('grey'),
        options: Common.colorsNull(),
      ),
      padding: k.options(
        label: 'padding',
        initial: null,
        options: Common.edgeInsetsNull(),
      ),
      dismissable: k.boolean(
        label: 'dismissable',
      ),
    ),
  ),
];

const _kBasicsSectionName = 'Flutter-Basics';
final flutterBasicsStories = [
  Story(
    name: 'Container',
    section: _kBasicsSectionName,
    builder: (context, k) => ContainerExample(
      color: k.options(
        label: 'color',
        initial: null,
        options: Common.colorsNull(),
      ),
      padding: k.options(
        label: 'padding',
        initial: null,
        options: Common.edgeInsetsNull(),
      ),
      margin: k.options(
        label: 'margin',
        initial: null,
        options: Common.edgeInsetsNull(),
      ),
    ),
  ),
];

const _kFlutterlSectionLayout = 'Flutter-Layout';
const _kFlutterMultiLayoutChild = 'Multi child';
const _kFlutterSingleLayoutChild = 'Single child';
final flutterLayoutStories = [
  Story(
    name: '$_kFlutterSingleLayoutChild: FittedBox',
    section: _kFlutterlSectionLayout,
    builder: (context, k) => FittedBoxExample(
      fit: k.options(
        label: 'fit (=BoxFit.contain)',
        initial: BoxFit.contain,
        options: [...BoxFit.values.map((e) => Option(e.toString(), e))],
      ),
      withPadding: k.options(
        label: 'with padding',
        initial: null,
        options: Common.edgeInsetsNull(),
      ),
    ),
  ),
  Story(
    name: '$_kFlutterMultiLayoutChild: Wrap',
    section: _kFlutterlSectionLayout,
    builder: (context, k) => WrapWidgetExample(
      count: k.sliderInt(
        label: 'with children count',
        initial: 5,
        min: 1,
        max: 100,
      ),
      direction: k.options(
        label: 'direction',
        initial: Axis.horizontal,
        options: [...Axis.values.map((e) => Option(e.toString(), e))],
      ),
      alignment: k.options(
        label: 'alignment',
        initial: WrapAlignment.start,
        options: [...WrapAlignment.values.map((e) => Option(e.toString(), e))],
      ),
      spacing: k.slider(
        label: 'spacing',
        initial: 10,
        min: 0,
        max: 150,
      ),
      runSpacing: k.slider(
        label: 'runSpacing',
        initial: 10,
        min: 0,
        max: 150,
      ),
    ),
  )
];
