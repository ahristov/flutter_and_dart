import 'package:flutter/material.dart';
import 'package:storybook_flutter/storybook_flutter.dart';

class Common {
  static List<Option<Color>> colors() => [
        Option('red', Colors.red),
        Option('green', Colors.green),
        Option('blue', Colors.blue),
        Option('orange', Colors.orange),
        Option('grey', Colors.grey.shade800),
        Option('white', Colors.white),
      ];

  static List<Option<Color?>> colorsNull() => [
        Option('', null),
        ...colors(),
      ];

  static Color? color(String colorName) => colorsNull().firstWhere((el) => el.text == colorName).value;

  static List<Option<EdgeInsets>> edgeInsets({List<double> values = const [20.0]}) => values
      .map((val) => [
            Option('EdgeInsets.all($val)', EdgeInsets.all(val)),
            Option('EdgeInsets.symmetric(vertical: $val)', EdgeInsets.symmetric(vertical: val)),
            Option('EdgeInsets.symmetric(horizontal: $val)', EdgeInsets.symmetric(horizontal: val)),
          ])
      .expand((el) => el)
      .toList();

  static List<Option<EdgeInsets?>> edgeInsetsNull({List<double> values = const [20, 30, 40, 50, 100, 500]}) => [
        Option('', null),
        ...edgeInsets(values: values),
      ];
}
