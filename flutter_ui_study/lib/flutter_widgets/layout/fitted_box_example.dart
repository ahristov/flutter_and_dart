import 'package:flutter/material.dart';

/*

## FittedBox widget

FittedBox widget: helps when one box does not fit in another.

Give it a `child` widget and a `fit`, which describes how the cild should fit.
It will scale or clip it's child automatically.

See: https://api.flutter.dev/flutter/widgets/FittedBox-class.html

*/

class FittedBoxExample extends StatelessWidget {
  final BoxFit fit;
  final EdgeInsetsGeometry? withPadding;
  const FittedBoxExample({
    required this.fit,
    this.withPadding,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: double.infinity,
      child: FittedBox(
        fit: fit,
        child: Container(
          child: Text(
            'Text in container',
            style: Theme.of(context).textTheme.headline5,
          ),
          color: Colors.red,
          padding: withPadding,
        ),
      ),
    );
  }
}
