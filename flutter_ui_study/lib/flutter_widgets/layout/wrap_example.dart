import 'package:flutter/material.dart';

/*

## Wrap widget

Wrap widget: like a Row or Column, but wraps it's children to next line.

Rows and Columns, sometimes run out of room.
Wrap widget lays out it's children one at a time lke a Row or Column,
but when it runs out of space, wraps to the next line.
You can use the `direction` property to tell whether to run vertically or horizontally.
There is `alignment` and `spacing` property as well.

See: https://api.flutter.dev/flutter/widgets/Wrap-class.html

*/

class WrapWidgetExample extends StatelessWidget {
  final int count;
  final Axis direction;
  final WrapAlignment alignment;
  final double spacing;
  final double runSpacing;

  const WrapWidgetExample({
    required this.count,
    required this.direction,
    required this.alignment,
    required this.spacing,
    required this.runSpacing,
  });

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Wrap(
        direction: direction,
        alignment: alignment,
        children: List.generate(
          count,
          (index) => _buildButton(index + 1),
        ),
        spacing: spacing,
        runSpacing: runSpacing,
      ),
    );
  }

  Widget _buildButton(int index) {
    return ElevatedButton(
      onPressed: () {},
      // child: Text('Button ${sprintf("%03i", [index])}'),
      child: Text('Button $index'),
    );
  }
}
