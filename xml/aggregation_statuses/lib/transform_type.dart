enum TransformType {
  translation,
  translationWithParams,
  enumeration,
}

extension TransformTypeExt on String? {
  TransformType get toTransformType {
    switch (this) {
      case 'translationWithParams':
        return TransformType.translationWithParams;
      case 'enumeration':
        return TransformType.enumeration;
      default:
        return TransformType.translation;
    }
  }
}
