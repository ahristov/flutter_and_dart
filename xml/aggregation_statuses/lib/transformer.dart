import 'dart:async';
import 'dart:io';

import 'package:xml/xml.dart';

import 'transform_type.dart';

const _kAccountAggregationDefaultErrorMessage = 'accountAggregationDefaultErrorMessage';

class Transformer {
  final String inputFileName;
  final TransformType transformType;

  Transformer({
    required this.inputFileName,
    required this.transformType,
  });

  Stream<String> transform() async* {
    final xmlString = await File(inputFileName).readAsString();
    final document = XmlDocument.parse(xmlString);

    yield* _handleDocument(document);
  }

  String _getElementKey(XmlElement element) =>
      element.attributes.first.value.replaceAll('.', '').lowerCaseFirstLetter();

  String _getElementText(XmlElement element) => element.innerText.trim();

  String _getElementStatusCode(XmlElement element) => element.attributes.first.value.split('.').last;

  List<String> _getEnumerationText(XmlElement element) {
    final String statusCode = _getElementStatusCode(element);
    final String key = statusCode == '0' ? _kAccountAggregationDefaultErrorMessage : _getElementKey(element);
    final res = <String>[];
    res.add("case '$statusCode': return localizations.$key.toBrandedString();");
    return res;
  }

  List<String> _getTranslationText(XmlElement element) {
    final String statusCode = _getElementStatusCode(element);
    final String txt = _getElementText(element);
    final String key = statusCode == '0' ? _kAccountAggregationDefaultErrorMessage : _getElementKey(element);
    final String description = statusCode == '0'
        ? 'Default account aggregation error message'
        : 'Account aggregation status message for status code $statusCode';
    final res = <String>[];
    res.add('"$key": "$txt",');
    if (transformType == TransformType.translationWithParams) {
      res.add('''"@$key": {
  "description": "$description"
},''');
    }
    return res;
  }

  Stream<String> _handleElement(XmlElement element) async* {
    if (element.localName.toLowerCase() != 'entry') {
      for (final child in element.childElements) {
        yield* _handleElement(child);
      }
    }

    if (element.attributes.isEmpty) {
      return;
    }

    if (transformType == TransformType.translation || transformType == TransformType.translationWithParams) {
      for (final s in _getTranslationText(element)) {
        yield s;
      }
    } else if (transformType == TransformType.enumeration) {
      for (final s in _getEnumerationText(element)) {
        yield s;
      }
    }
  }

  Stream<String> _handleDocument(XmlDocument document) async* {
    for (final element in document.childElements) {
      yield* _handleElement(element);
    }
  }
}

extension StringExtension on String {
  String lowerCaseFirstLetter() => "${this[0].toLowerCase()}${substring(1)}";
}
