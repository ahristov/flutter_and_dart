A command-line application that transforms aggregated accolunt statuses.

To get the English texts:

```sh
dart run aggregation_statuses "data/input_en.xml" true
```

To get the Spanish texts:

```sh
dart run aggregation_statuses "data/input_es.xml" false
```

Directories:

- entrypoint in `bin/`
- library code in `lib/`
- inpout xml files in `data/`
- example unit test in `test/`

