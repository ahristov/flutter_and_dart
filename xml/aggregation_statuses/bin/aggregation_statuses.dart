import 'package:aggregation_statuses/transformer.dart';
import 'package:aggregation_statuses/transform_type.dart';

void main(List<String> arguments) {
  if (arguments.length < 2) {
    usage();
  }

  final inputFileName = arguments.isNotEmpty ? arguments[0] : 'data/input_en.xml';
  final transformType = (arguments.length >= 2 ? arguments[1] : '').toTransformType;

  final transformer = Transformer(
    inputFileName: inputFileName,
    transformType: transformType,
  );

  transformer.transform().listen((s) {
    print(s);
  });
}

void usage() {
  print('''
Generate i10n texts from xml files.

To generate the English texts run:

dart run aggregation_statuses "data/input_en.xml" translationWithParams

To generate the Spanish texts run:

dart run aggregation_statuses "data/input_es.xml" translation

To generate the enumeration Dart code run:

dart run aggregation_statuses "data/input_en.xml" enumeration

  ''');
}
