# Flutter and Dart

This project contains code from studying Flutter and Dart

## 🏠 [Homepage](https://bitbucket.org/ahristov/flutter_and_dart)

## Projects

[Dart: Intro](./dart_intro)

[Flutter: i_am_rich](./i_am_rich)

## Documents

[Syllabus - The Complete Flutter Development Bootcamp](./doc/Flutter+Dev+Syllabus.pdf)

## Resources

[Udemy Flutter course resources](https://github.com/londonappbrewery/Flutter-Course-Resources)

[Learn to type faster](https://www.keybr.com/)

[Codemagic: Flutter automate, build and deploy pipeline](https://codemagic.io/)

[Material design](https://material.io/)

[Generate icons for different platforms](https://appicon.co/)

## Notes

Useful `flutter` commands:

- see flutter version

```
flutter --version
```

- upgrade flutter:

```
flutter upgrade
```

- check flutter installation:

```
flutter doctor
```