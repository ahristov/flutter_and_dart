# Analytics filter v2

Project created with:

```sh
dart create -t console-simple --force .
```

To generate json serializable run:

```sh
flutter pub run build_runner build --delete-conflicting-outputs
```

To run unit tests run:

```sh
dart test
# or #
dart test -r expanded
```

To run test coverage report

```sh
flutter test --coverage ; genhtml coverage/lcov.info -o coverage/html ; open coverage/html/index.html
```
