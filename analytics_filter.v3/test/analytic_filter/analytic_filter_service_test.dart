import 'package:analytics_filter/analytic_event/analytic_event.dart';
import 'package:analytics_filter/analytic_filter/analytic_filter_service.dart';
import 'package:analytics_filter/analytic_filter/filter_config.dart';
import 'package:analytics_filter/analytic_filter/filter_config_type.dart';
import 'package:test/test.dart';

const _kEmptyConfigFilePath = 'test/analytic_filter/assets/analytic_filter_empty.json';
const _kEmptyJsonConfigFile = 'test/analytic_filter/assets/analytic_filter_empty_json.json';
const _kEmptyAllowRulesConfigFile = 'test/analytic_filter/assets/analytic_filter_empty_allow_rules.json';
const _kEmptyDenyRulesConfigFile = 'test/analytic_filter/assets/analytic_filter_empty_deny_rules.json';
const _kAllowAccountsConfigFile = 'test/analytic_filter/assets/analytic_filter_allow_accounts.json';
const _kDenyAccountsConfigFile = 'test/analytic_filter/assets/analytic_filter_deny_accounts.json';

const _kAccountActions = 'account_actions';
const _kAccounts = 'accounts';
const _kAccountSelected = 'account_selected';
const _kAction = 'action';
const _kActivityShown = 'activity_shown';
const _kBillPay = 'bill_pay';
const _kPayNow = 'pay_now';
const _kRemoteDeposit = 'remote_deposit';
const _kSelected = 'selected';
const _kTransfer = 'transfers';

final _eventAccountActionTransfer =
    const AnalyticEvent(eventName: _kAccountActions, eventParameters: {'n1': 'v1', _kAction: _kTransfer, 'n2': 'v2'});

final _eventAccountActionPayNow =
    const AnalyticEvent(eventName: _kAccountActions, eventParameters: {'n1': 'v1', _kAction: _kPayNow});

final _eventAccountActionRemoteDeposit =
    const AnalyticEvent(eventName: _kAccountActions, eventParameters: {_kAction: _kRemoteDeposit, 'n2': 'v2'});

final _eventAccountActionBillPay =
    const AnalyticEvent(eventName: _kAccountActions, eventParameters: {_kAction: _kBillPay});

final _eventAccountsActionSelected =
    const AnalyticEvent(eventName: _kAccounts, eventParameters: {_kAction: _kSelected});

final _eventRemoteDepositActionAccountSelected =
    const AnalyticEvent(eventName: _kRemoteDeposit, eventParameters: {_kAction: _kAccountSelected});

final _eventRemoteDepositActionActivityShown =
    const AnalyticEvent(eventName: _kRemoteDeposit, eventParameters: {_kAction: _kActivityShown});

Future _assertEventWithConfigFile<T>(T expected, AnalyticEvent event, String configFilePath) async {
  final filter = AnalyticFilterService.withConfigFilePath(configFilePath);
  final actual = await filter.shouldInclude(event);
  expect(actual, expected);
}

Future _assertEventsWithConfigFile<T>(T expected, List<AnalyticEvent> events, String configFilePath) async {
  for (final event in events) {
    await _assertEventWithConfigFile(expected, event, configFilePath);
  }
}

Future _assertWithConfigData<T>(T expected, AnalyticEvent event, FilterConfig configData) async {
  final filter = AnalyticFilterService.withConfigData(configData);
  final actual = await filter.shouldInclude(event);
  expect(actual, expected);
}

Future main() async {
  group('Init with config data:', () {
    test(
        'Empty allow rules should exclude the event',
        () async => await _assertWithConfigData(
            false, _eventAccountActionTransfer, FilterConfig(filterType: FilterConfigType.allow)));

    test(
        'Empty deny rules should include the event',
        () async => await _assertWithConfigData(
            true, _eventAccountActionTransfer, FilterConfig(filterType: FilterConfigType.deny)));
  });
  group('Init with config file:', () {
    test('Missing config file should include the event',
        () async => await _assertEventWithConfigFile(true, _eventAccountActionTransfer, 'file-does-bit-exist'));

    test('Empty config file should include the event',
        () async => await _assertEventWithConfigFile(true, _eventAccountActionTransfer, _kEmptyConfigFilePath));

    test('Empty config JSON should include the event',
        () async => await _assertEventWithConfigFile(true, _eventAccountActionTransfer, _kEmptyJsonConfigFile));

    test('Empty allow rules should exclude the event',
        () async => await _assertEventWithConfigFile(false, _eventAccountActionTransfer, _kEmptyAllowRulesConfigFile));

    test('Empty deny rules should include the event',
        () async => await _assertEventWithConfigFile(true, _eventAccountActionTransfer, _kEmptyDenyRulesConfigFile));

    test(
        'Test allow accounts config file should include account actions',
        () async => await _assertEventsWithConfigFile(
            true,
            [
              _eventAccountActionTransfer,
              _eventAccountActionPayNow,
              _eventAccountActionRemoteDeposit,
              _eventAccountsActionSelected,
              const AnalyticEvent(eventName: _kAccounts),
              const AnalyticEvent(eventName: _kAccounts, eventParameters: {}),
              const AnalyticEvent(eventName: _kAccounts, eventParameters: {'n': 'v'}),
            ],
            _kAllowAccountsConfigFile));

    test('Test allow accounts config file should exclude action bill_pay',
        () async => await _assertEventWithConfigFile(false, _eventAccountActionBillPay, _kAllowAccountsConfigFile));

    test(
        'Test allow accounts config file should exclude remote deposit actions',
        () async => await _assertEventsWithConfigFile(
            false,
            [
              _eventRemoteDepositActionAccountSelected,
              _eventRemoteDepositActionActivityShown,
            ],
            _kAllowAccountsConfigFile));

    test(
        'Test deny accounts config file should exclude account actions',
        () async => await _assertEventsWithConfigFile(
            false,
            [
              _eventAccountActionTransfer,
              _eventAccountActionPayNow,
              _eventAccountActionRemoteDeposit,
              _eventAccountsActionSelected,
              const AnalyticEvent(eventName: _kAccounts),
              const AnalyticEvent(eventName: _kAccounts, eventParameters: {}),
              const AnalyticEvent(eventName: _kAccounts, eventParameters: {'n': 'v'}),
            ],
            _kDenyAccountsConfigFile));

    test('Test deny accounts config file should include action bill_pay',
        () async => await _assertEventWithConfigFile(true, _eventAccountActionBillPay, _kDenyAccountsConfigFile));

    test(
        'Test deny accounts config file should include remote deposit actions',
        () async => await _assertEventsWithConfigFile(
            true,
            [
              _eventRemoteDepositActionAccountSelected,
              _eventRemoteDepositActionActivityShown,
            ],
            _kDenyAccountsConfigFile));
  });
}
