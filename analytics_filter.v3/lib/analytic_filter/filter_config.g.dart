// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filter_config.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FilterConfig _$FilterConfigFromJson(Map<String, dynamic> json) => FilterConfig(
      filterType:
          $enumDecodeNullable(_$FilterConfigTypeEnumMap, json['filterType']),
      filterRules: (json['filterRules'] as List<dynamic>?)
              ?.map((e) => FilterRule.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
    );

Map<String, dynamic> _$FilterConfigToJson(FilterConfig instance) =>
    <String, dynamic>{
      'filterType': _$FilterConfigTypeEnumMap[instance.filterType],
      'filterRules': instance.filterRules,
    };

const _$FilterConfigTypeEnumMap = {
  FilterConfigType.allow: 'allow',
  FilterConfigType.deny: 'deny',
};
