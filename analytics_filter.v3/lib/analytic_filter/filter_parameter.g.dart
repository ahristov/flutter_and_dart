// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filter_parameter.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FilterParameter _$FilterParameterFromJson(Map<String, dynamic> json) =>
    FilterParameter(
      eventParameterName: json['eventParameterName'] as String?,
      eventParameterValues: (json['eventParameterValues'] as List<dynamic>?)
              ?.map((e) => e as String)
              .toList() ??
          const [],
    );

Map<String, dynamic> _$FilterParameterToJson(FilterParameter instance) =>
    <String, dynamic>{
      'eventParameterName': instance.eventParameterName,
      'eventParameterValues': instance.eventParameterValues,
    };
