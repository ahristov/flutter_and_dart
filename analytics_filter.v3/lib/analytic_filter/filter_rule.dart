import 'package:json_annotation/json_annotation.dart';

import 'filter_parameter.dart';

part 'filter_rule.g.dart';

@JsonSerializable()
class FilterRule {
  final String? eventName;
  final List<FilterParameter>? eventParameters;

  FilterRule({this.eventName, this.eventParameters = const []});

  factory FilterRule.fromJson(Map<String, dynamic> json) => _$FilterRuleFromJson(json);

  Map<String, dynamic> toJson() => _$FilterRuleToJson(this);
}
