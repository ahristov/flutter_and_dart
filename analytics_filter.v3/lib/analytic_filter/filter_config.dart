import 'package:json_annotation/json_annotation.dart';

import 'filter_config_type.dart';
import 'filter_rule.dart';

part 'filter_config.g.dart';

@JsonSerializable()
class FilterConfig {
  final FilterConfigType? filterType;
  final List<FilterRule>? filterRules;

  FilterConfig({this.filterType, this.filterRules = const []});

  factory FilterConfig.fromJson(Map<String, dynamic> json) => _$FilterConfigFromJson(json);

  Map<String, dynamic> toJson() => _$FilterConfigToJson(this);
}
