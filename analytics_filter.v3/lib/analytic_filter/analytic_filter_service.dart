import 'dart:convert';
import 'dart:io';

import '../analytic_event/analytic_event.dart';
import 'filter_config.dart';
import 'filter_config_type.dart';
import 'filter_parameter.dart';
import 'filter_rule.dart';

abstract class AnalyticFilterServiceBase {
  Future<bool> shouldInclude(AnalyticEvent event);
}

class AnalyticFilterService extends AnalyticFilterServiceBase {
  late final FilterConfig filterConfig;
  late final Future _initialized;

  /// Create instance and load data from the specified JSON config file [configFilePath].
  AnalyticFilterService.withConfigFilePath(String configFilePath) {
    _initialized = _init(configFilePath);
  }

  /// Create instance and initialize with the specified data [filterConfig].
  AnalyticFilterService.withConfigData(this.filterConfig) {
    _initialized = Future.value();
  }

  @override
  Future<bool> shouldInclude(AnalyticEvent event) async {
    await _initialized;

    if (filterConfig.filterType == null) {
      return true;
    }

    final rulesApply = _rulesApplyToEvent(event, filterConfig.filterRules ?? []);
    return filterConfig.filterType == FilterConfigType.allow ? rulesApply : !rulesApply;
  }

  bool _rulesApplyToEvent(AnalyticEvent event, List<FilterRule> rules) {
    final eventName = event.eventName.toLowerCase();

    for (final rule in rules) {
      final ruleEventName = (rule.eventName ?? '').toLowerCase();
      if (ruleEventName.isEmpty) {
        continue;
      }
      if (ruleEventName == eventName) {
        final eventParams = event.eventParameters ?? {};
        final ruleParams = rule.eventParameters ?? [];
        final ruleApplies = _rulesApplyToEventParams(eventParams, ruleParams);
        if (ruleApplies) {
          return true;
        }
      }
    }

    return false;
  }

  bool _rulesApplyToEventParams(Map<String, String> eventParams, List<FilterParameter> ruleParams) {
    if (ruleParams.isEmpty) {
      return true;
    }

    if (eventParams.isEmpty) {
      return false;
    }

    for (final ruleParam in ruleParams) {
      final ruleParamName = (ruleParam.eventParameterName ?? '').toLowerCase();
      final eventParam = eventParams[ruleParamName];
      if (eventParam == null) {
        continue;
      }

      final ruleParamValues = ruleParam.eventParameterValues ?? [];
      if (ruleParamValues.isEmpty) {
        return true;
      }

      if (ruleParamValues.contains(eventParam)) {
        return true;
      }
    }

    return false;
  }

  @override
  Future _init(String configFilePath) async {
    filterConfig = await _loadConfigData(configFilePath);
  }

  Future<String> _loadFileAsString(String filePath) async =>
      await File(filePath).exists() ? await File(filePath).readAsString() : '';

  Future<FilterConfig> _loadConfigData(String filePath) async {
    final jsonString = await _loadFileAsString(filePath);
    if (jsonString.isEmpty) {
      return FilterConfig();
    }

    final Map<String, dynamic> jsonContent = json.decode(jsonString) as Map<String, dynamic>;
    final result = FilterConfig.fromJson(jsonContent);
    return result;
  }
}
