// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filter_rule.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FilterRule _$FilterRuleFromJson(Map<String, dynamic> json) => FilterRule(
      eventName: json['eventName'] as String?,
      eventParameters: (json['eventParameters'] as List<dynamic>?)
              ?.map((e) => FilterParameter.fromJson(e as Map<String, dynamic>))
              .toList() ??
          const [],
    );

Map<String, dynamic> _$FilterRuleToJson(FilterRule instance) =>
    <String, dynamic>{
      'eventName': instance.eventName,
      'eventParameters': instance.eventParameters,
    };
