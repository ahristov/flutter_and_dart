import 'package:json_annotation/json_annotation.dart';

part 'filter_parameter.g.dart';

@JsonSerializable()
class FilterParameter {
  final String? eventParameterName;
  final List<String>? eventParameterValues;

  FilterParameter({this.eventParameterName, this.eventParameterValues = const []});

  factory FilterParameter.fromJson(Map<String, dynamic> json) => _$FilterParameterFromJson(json);

  Map<String, dynamic> toJson() => _$FilterParameterToJson(this);
}
