import 'package:analytics_filter/analytic_event/analytic_providers.dart';

class AnalyticEvent {
  static const kDefaultAnalyticProviders = [
    AnalyticProviders.adobeAnalytics,
    AnalyticProviders.apptentive,
    AnalyticProviders.firebase,
  ];

  final String eventName;
  final Map<String, String>? eventParameters;
  final AnalyticProviders eventProviders;

  const AnalyticEvent(
      {required this.eventName,
      this.eventParameters,
      this.eventProviders = const AnalyticProviders(providers: kDefaultAnalyticProviders)});

  const AnalyticEvent.experience(
      {required this.eventName,
      this.eventParameters,
      this.eventProviders = const AnalyticProviders(providers: kDefaultAnalyticProviders)});
}
