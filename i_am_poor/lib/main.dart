import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.purple[200],
        appBar: AppBar(
          title: Text('I am poor'),
          backgroundColor: Colors.pink[500],
        ),
        body: Center(
          child: Image(
            image: AssetImage(
              'images/heart.png',
            ),
          ),
        ),
      ),
    ),
  );
}
