import 'dart:io' show Platform;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tony_bitcoin_ticker/coin_data.dart';

class PriceScreen extends StatefulWidget {
  @override
  _PriceScreenState createState() => _PriceScreenState();
}

class _PriceScreenState extends State<PriceScreen> {
  String selectedCurrency = defaultCurrency;
  Map<String, String> cryptoRates = {};

  @override
  void initState() {
    super.initState();
    cryptoList.forEach((element) {
      cryptoRates[element] = '';
    });
    updateCoinRates();
  }

  Future<void> updateCoinRates() async {
    setState(() {
      cryptoList.forEach((coin) {
        cryptoRates[coin] = '';
      });
    });
    cryptoList.forEach((coin) async {
      var lastRate = await CoinData().getCoinExchangeRate(
        coin: coin,
        currency: selectedCurrency,
      );
      setState(() {
        cryptoRates[coin] = lastRate.toStringAsFixed(0);
      });
    });
  }

  DropdownButton<String> getAndroidDropdown() {
    var dropdownItems = <DropdownMenuItem<String>>[];
    for (String currency in currenciesList) {
      var newItem = DropdownMenuItem(
        child: Text(currency),
        value: currency,
      );
      dropdownItems.add(newItem);
    }

    return DropdownButton<String>(
      value: selectedCurrency,
      items: dropdownItems,
      onChanged: (value) async {
        setState(() {
          selectedCurrency = value.toString();
        });
        updateCoinRates();
      },
    );
  }

  CupertinoPicker getIOSPicker() {
    var widgetItems = <Widget>[];
    for (String currency in currenciesList) {
      var newItem = Text(currency);
      widgetItems.add(newItem);
    }

    return CupertinoPicker(
      itemExtent: 32.0,
      backgroundColor: Colors.lightBlue,
      onSelectedItemChanged: (selectedIndex) {
        setState(() {
          selectedCurrency = currenciesList[selectedIndex];
        });
        updateCoinRates();
      },
      children: widgetItems,
    );
  }

  List<Widget> getListOfCoins() {
    var widgetItems = <Widget>[];
    cryptoList.forEach((crypto) {
      widgetItems.add(Padding(
        padding: EdgeInsets.fromLTRB(18.0, 18.0, 18.0, 0),
        child: Card(
          color: Colors.lightBlueAccent,
          elevation: 5.0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 28.0),
            child: Text(
              cryptoRates[crypto].toString().isNotEmpty
                  ? '1 $crypto = ${cryptoRates[crypto]} $selectedCurrency'
                  : '1 $crypto = ? $selectedCurrency',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20.0,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ));
    });
    return widgetItems;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('🤑 Coin Ticker'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: getListOfCoins(),
            ),
          ),
          Container(
            height: 150.0,
            alignment: Alignment.center,
            padding: EdgeInsets.only(bottom: 30.0),
            color: Colors.lightBlue,
            child: Platform.isIOS ? getIOSPicker() : getAndroidDropdown(),
          ),
        ],
      ),
    );
  }
}
