import 'dart:convert';

import 'package:http/http.dart' as http;

class NetworkHelper {
  final String uri;
  final Map<String, String>? headers;

  NetworkHelper({this.uri = '', this.headers});

  Future getData() async {
    var url = Uri.parse(uri);
    final response = await http.get(url, headers: this.headers ?? {});
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      final msg = 'Could not fetch data, status: ${response.statusCode}.';
      print(msg);
      throw msg;
    }
  }
}
