import 'networking.dart';

const List<String> currenciesList = [
  'AUD',
  'BRL',
  'CAD',
  'CNY',
  'EUR',
  'GBP',
  'HKD',
  'IDR',
  'ILS',
  'INR',
  'JPY',
  'MXN',
  'NOK',
  'NZD',
  'PLN',
  'RON',
  'RUB',
  'SEK',
  'SGD',
  'USD',
  'ZAR'
];

const List<String> cryptoList = [
  'BTC',
  'ETH',
  'LTC',
];

const defaultCurrency = 'USD';

const _apiKey = '93B0DA84-26EA-4569-9E79-DE2D323A137B';
const _coinExchangeRateUrl = 'https://rest.coinapi.io/v1/exchangerate';

class CoinData {
  Future<double> getCoinExchangeRate(
      {required String coin, required String currency}) async {
    String requestUrl = '$_coinExchangeRateUrl/$coin/$currency';
    NetworkHelper networkHelper = NetworkHelper(uri: requestUrl, headers: {
      'X-CoinAPI-Key': _apiKey,
    });
    var exchangeData = await networkHelper.getData();
    double lastRate = exchangeData['rate'];
    return lastRate;
  }
}
