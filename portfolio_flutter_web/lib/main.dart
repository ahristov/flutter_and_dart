import 'package:flutter/material.dart';

import 'desktop/desktop_view.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Tony Hristov',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const DesktopView());
  }
}
