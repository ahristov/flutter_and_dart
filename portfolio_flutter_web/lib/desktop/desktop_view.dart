import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class DesktopView extends StatelessWidget {
  const DesktopView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 864,
              width: 1200,
              child: Row(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'I\'m a Mobile',
                        style: GoogleFonts.montserrat(
                          fontSize: 60,
                        ),
                      ),
                      Text(
                        'Developer < / >',
                        style: GoogleFonts.montserrat(
                          fontSize: 60,
                        ),
                      ),
                      const SizedBox(height: 30),
                      SizedBox(
                        width: 600,
                        child: Text(
                          'I have a 4 months of experience in mobile development building beautiful apps in Flutter.',
                          style: GoogleFonts.openSans(
                            fontSize: 24,
                          ),
                          maxLines: 100,
                        ),
                      ),
                      const SizedBox(height: 40),
                      TextButton(
                        style: TextButton.styleFrom(
                          primary: Colors.white,
                          backgroundColor: Colors.redAccent,
                          elevation: 10,
                          shape: const RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8.0)),
                          ),
                          padding: const EdgeInsets.symmetric(vertical: 27, horizontal: 25),
                          minimumSize: const Size(88, 36),
                        ),
                        onPressed: () {},
                        child: Text(
                          'Contact Me',
                          style: GoogleFonts.openSans(
                            fontSize: 20,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const FlutterLogo(size: 300),
                ],
              ),
            ),
            Container(height: height, width: width, color: Colors.blue),
          ],
        ),
      ),
    );
  }
}
