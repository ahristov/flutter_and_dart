/*
    BLOC
 */

import 'package:bloc/bloc.dart';

void runBlocExample() {
  _counterCounterCubitExample();
}

class CounterCubit extends Cubit<int> {
  CounterCubit() : super(0);
  void increment() => emit(state + 1);
  void decrement() => emit(state - 1);
}

void _counterCounterCubitExample() async {
  print('Cubit - Counter cubit example:');

  final cubit = CounterCubit();

  final streamSubscription = cubit.stream.listen((event) {
    //! This subscribes to the cubit state and prints the state value emmited by it
    print('Subscriber event: Counter is at: $event');
  });

  print('Cubit state: Counter is at ${cubit.state}');

  cubit.increment();
  print('Cubit state: Counter is at ${cubit.state}');

  cubit.increment();
  print('Cubit state: Counter is at ${cubit.state}');

  cubit.decrement();
  print('Cubit state: Counter is at ${cubit.state}');

  await Future.delayed(Duration.zero); //! we use this to not cancel the subscription immediately down here
  await streamSubscription.cancel();
  await cubit.close();
}
