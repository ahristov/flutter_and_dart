import 'dart:async';

import 'package:get_it/get_it.dart';

void runMixinsExample() {
  print('Begin runMixinsExample');
  CoreAdobeAnalyticsService().handleEvent(
    AnalyticEvent(
      eventName: 'accounts',
      eventParameters: {
        'action': 'submit',
      },
    ),
  );
  print('End runMixinsExample');
}

abstract class AdobeAnalyticsServiceBase {
  bool shouldSkip(AnalyticEvent event) {
    return true;
  }

  void handleEvent(AnalyticEvent event);
}

abstract class AdobeAnalyticsFilter {
  bool shouldSkip(AnalyticEvent event) {
    print('AdobeAnalyticsFilter#shouldSkip');
    return true;
  }
}

class CoreAdobeAnalyticsService extends AdobeAnalyticsServiceBase implements Disposable {
  @override
  void handleEvent(AnalyticEvent event) {
    if (shouldSkip(event)) {
      return;
    }
    print('CoreAdobeAnalyticsService#handleEvent');
  }

  @override
  FutureOr onDispose() {
    print('CoreAdobeAnalyticsService#onDispose');
  }
}

class AnalyticEvent {
  static const kDefaultAnalyticProviders = [
    AnalyticProviders.adobeAnalytics,
    AnalyticProviders.apptentive,
    AnalyticProviders.firebase,
  ];

  final String eventName;
  final Map<String, String>? eventParameters;
  final AnalyticProviders eventProviders;

  const AnalyticEvent(
      {required this.eventName,
      this.eventParameters,
      this.eventProviders = const AnalyticProviders(providers: kDefaultAnalyticProviders)});

  const AnalyticEvent.experience(
      {required this.eventName,
      this.eventParameters,
      this.eventProviders = const AnalyticProviders(providers: kDefaultAnalyticProviders)});
}

class AnalyticProviders {
  static const firebase = 1;
  static const apptentive = 2;
  static const adobeAnalytics = 4;

  final List<int> providers;

  /// Returns the bitmask of the providers passed in
  int get providersBitmask => providers.sum;

  const AnalyticProviders({required this.providers});
}

/// Extensions that apply to iterables of numbers.
extension IterableNumberExtension on Iterable<int> {
  /// The sum of the elements.
  ///
  /// The sum is zero if the iterable is empty.
  int get sum {
    var result = 0;
    for (var value in this) {
      result += value;
    }
    return result;
  }
}
