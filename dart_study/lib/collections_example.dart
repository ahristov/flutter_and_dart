/*
    Collections
 */

void runCollectionsExample() {
  enumExample();
  listExample();
}

// Enums have to be declared outside functions.
enum colors { red, yellow, green }

void enumExample() {
  print('Collections - Enums:');

  print(colors); // Prints "colors", because enums are variables. they do not have `.runtimeType`
  print(colors.red); // Prints "colors.red".
  print(colors.values); // Prints "[colors.red, colors.green, colors.blue]"

  const color = colors.green;
  switch (color) {
    case colors.red:
    case colors.yellow:
      print('Red or yellow!');
      break;
    case colors.green:
      print('Green.');
      break;
    default:
      print('Traffic light not working!');
  }
}

void listExample() {
  print('Collections - Lists:');

  const list1 = [1, 2, 3, 4];
  print(list1.runtimeType); // Prints "List<int>"
  print(list1); // Prints "[1, 2, 3, 4]"
  print(list1.length); // Prints "4"
  print('First item is ${list1[0]}'); // Prints "First item is 1"

  // var list2 = List(); // Convert to literals when possible
  var list3 = []; // Creates instance of List<dynamic>
  list3.add('One');
  list3.add(2);
  print(list3); // Prints "[One, 2]"

  // print unique and sorted the non null sub-elements
  print(([
    [1],
    null,
    [],
    [null, 5, 4, 3, null],
    [3, 4, 5],
    [2, 3]
  ].expand((item) => item ?? []).where((el) => el != null).toSet().toList()
        ..sort())
      .join(',')); // prints: 1,2,3,4,5
}
