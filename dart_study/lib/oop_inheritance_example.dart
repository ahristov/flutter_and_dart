void runOOPInheritanceExample() {
  // showInheritance();
  // showMethodOverride();
  // showInheritClassesWithConstructor();
  showOverrideToString();
}

void showInheritance() {
  var bonni = Bonni();
  bonni.firstName = 'Bonni';
  bonni.showName();
  bonni.showProfession();

  var paulo = Paulo();
  paulo.firstName = 'Paulo';
  paulo.age = 46;
  bonni.showName();
  print(paulo.age);
}

void showMethodOverride() {
  var bonni = Bonni();
  bonni.firstName = 'Bonni';
  bonni.sayHello();

  var paulo = Paulo();
  paulo.firstName = 'Paulo';
  paulo.sayHello();
}

void showInheritClassesWithConstructor() {
  var location1 = Location(1.2, 3.4);
  location1.showLocation();

  var location2 = ElevatedLocation(-1.2, -3.4, 150);
  location2.showLocation();
}

void showOverrideToString() {
  var object = Object();
  print(object.toString());

  var s = 'This is a string';
  print(s.toString());

  var location = ElevatedLocation(-1.2, -3.4, 150);
  print(location);
}

//

class Location {
  num lat, lng;

  Location(this.lat, this.lng);

  // Named constructor
  Location.create(this.lat, this.lng);

  void showLocation() => print('Location: lat=${lat}, lng:${lng}');
}

class ElevatedLocation extends Location {
  num elv;

  ElevatedLocation(num lat, num lng, this.elv) : super(lat, lng);

  // or we can use named constructors as well:
  // ElevatedLocation(num lat, num lng, this.elv) : super.create(lat, lng);

  @override
  void showLocation() {
    print(toString());
  }

  @override
  String toString() => 'Location:lat=${lat}, lng:${lng}, elv=${elv}';
}

class Person {
  String? firstName, lastName, nationality;
  int? age;

  void showName() {
    print(firstName);
  }

  void sayHello() {
    print('Hello');
  }
}

class Bonni extends Person {
  String? profession;

  void showProfession() => print(profession);
}

class Paulo extends Person {
  bool? playGuitar;

  @override
  void sayHello() {
    // TODO: implement sayHello
    // super.sayHello();
    print('Ola');
  }
}
