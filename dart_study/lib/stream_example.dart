/*
    Streams
 */

void runStreamExample() {
  _streamRiverExample();
}

void _streamRiverExample() {
  print('Streams - River example:');

  Stream<int> boatStream() async* {
    for (var i = 1; i <= 10; i++) {
      print('SENT boat No $i');
      await Future.delayed(Duration(seconds: 2));
      yield i;
    }
  }

  final stream = boatStream();

  stream.listen((boatNumber) {
    print('RECEIVED boat No $boatNumber');
  });
}
