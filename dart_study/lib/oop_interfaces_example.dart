void runOOPInterfacesExample() {
  Animal cat = Cat();
  cat.breathe();
  cat.makeNoise();

  Animal tiger = Tiger();
  tiger.breathe();
  tiger.makeNoise();

  var person = Person('John Doe', 'American');
  print(person);
  person.breathe();
  person.makeNoise();

  var comedian = Comedian('Mr. Bean', 'British');
  print('${comedian}. Is funny? ${comedian.getIsFunny()}');
}

abstract class Animal {
  void breathe(); // abstract method
  void makeNoise() => print('Making animal noises!');
}

abstract class Feline extends Animal {
  @override
  void breathe() => print('Feline breathe');
}

class Cat extends Feline {
  @override
  void makeNoise() => print('Says a meow...');
}

class Tiger extends Feline {
  @override
  void makeNoise() => print('Says a roar!!!');
}

class Person extends Animal {
  String name, nationality;

  Person(this.name, this.nationality);

  @override
  void breathe() => print('People breathe');

  @override
  void makeNoise() => print('People make noise!');

  @override
  String toString() => '${name}, ${nationality}';
}

abstract class IsFunny {
  bool getIsFunny();
}

class Comedian extends Person implements IsFunny {
  Comedian(String name, String nationality) : super(name, nationality);

  @override
  bool getIsFunny() => true;
}

class Clown extends Person implements IsFunny {
  Clown(String name, String nationality) : super(name, nationality);

  @override
  bool getIsFunny() => true;
}

class TVShow implements IsFunny {
  bool? _isFunny;

  TVShow(bool isFunny) {
    _isFunny = isFunny;
  }

  @override
  bool getIsFunny() => true;
}
