/*
    Types:
    - String (String)
    - Number (num, int, double)
    - Boolean (bool)
    - lists
    - maps
    ...

    Const and final keywords:
    const - when we want the variable value to be set at compile-time
    final - set at runtime, but never changes once set

    Operators (+, -, *, /, %)
    % = reminder - what remains from a division operation.
    dividend / divisor = quotient R reminder

    Equality and relational operators ( ==, !=, >, <, >=, <= )

    Type relational operators (as, is, is!)

    Logical operators ( !, &&, || )

 */

void runVariablesExample() {
  dynamicTypeExample();
  inferredTypeExample();
  explicitlyDeclaredStringExample();
  stringConcatenationExample();
  numberExample();
  booleanExample();
  constAndFinalExample();
  mathematicalOperatorsExample();
  equalityAndRelationalOperatorsExample();
  typeRelationalOperatorsExample();
  logicalOperatorsExample();
  logicalOperatorsExample();
  nullSafetyExample();
}

/*
  Variable of dynamic type - declare `var` variable without assigning value of some type.
 */
void dynamicTypeExample() {
  print('Variables - Variable of dynamic type example:');

  var something;
  something = 'Something like a string';
  something = 12.34;
  something = 12345;
  print('${something * 2 + 1}');
}

/*
  Inferred type - declare and initialize a variable.
 */
void inferredTypeExample() {
  print('Variables - Inferred type example:');

  var country = 'Bulgaria'; // String
  // country = 123; // Error: A value of type 'int' can't be assigned to a variable of type 'String'.
  print('Country is $country');
}

/*
  Explicitly declared type
 */
void explicitlyDeclaredStringExample() {
  print('Variables - Explicitly declared type example:');

  String city, country;
  city = 'Kubrat';
  country = 'Bulgaria';
  // country = 123; // Error: A value of type 'int' can't be assigned to a variable of type 'String'.
  print('$city, ${country}');
}

/*
  String concatenation
 */
void stringConcatenationExample() {
  print('Variables - String concatenation example:');

  var fName = 'James';
  var lName = 'Bond';
  var fullName = '$fName $lName';

  // Get an index.
  print('$fName ${lName.toUpperCase()} is ${50 + 1}y old.');

  // Get a substring.
  print('${fName.substring(0, 3)} $lName is ${50 + 1}y old.');

  // Get an index of a substring.
  var idx = fullName.indexOf(' ');
  print('${fullName.substring(idx).trim()}');

  // Split a string
  var names = fullName.split(' ');
  // print('First name: $names[0]'); // First name: [James, Bond][0]
  print('First name: ${names[0]}');
  print('Last name: ${names[1]}');
}

/*
  Number (num, int, double)
 */
void numberExample() {
  //
  print('Variables - Number example:');

  num number = 13;
  number = 123.45;
  var age = 51;
  // age = 12.34; // Error: A value of type 'double' can't be assigned to a variable of type 'int'.

  // Parse an integer. The result of this is 51.
  // Default radix is 10.
  age = int.tryParse('00051.999', radix: 10) ?? 0;

  // Parse an integer. Since the parsing failed, result will be `null`.
  // age = int.tryParse('x');

  // This will throw "FormatException: Invalid radix-10 number"
  // age = int.parse('x');

  // This will handle the error by returning value of 18.
  // age = int.parse('x', onError: (source) => 18);

  var salary = 3456.55;
  salary = 4567.89;
  print('Age: $age, salary: \$$salary, bonus: \$${234.56 + 12.34}');
}

/*
  Boolean (bool)
 */
void booleanExample() {
  print('Variables - Boolean example:');

  var isTrue = true;
  var isFalse = false;
  print(isFalse);
  print('$isTrue, $isFalse');

  bool isOn;
  // print('isOn = ${isOn}'); // Produces null
  // print(
  //     'isOn has runtimeType of ${isOn.runtimeType}'); // Everything is an object, produces `Null`

  isOn = true;
  print('isOn = ${isOn}');
  print('isOn has runtimeType of ${isOn.runtimeType}'); // Everything is an object, produces `bool`

  isOn = false;
  print('isOn = ${isOn}');
  print('isOn has runtimeType of ${isOn.runtimeType}'); // Everything is an object, produces `bool`
}

/*
  Const and final keywords
  const - when we want the variable value to be set at compile-time
  final - set at runtime, but never changes once set
 */
void constAndFinalExample() {
  print('Variables - Const and final keywords example:');

  const pi = 3.14;
  // pi = 2.34; // Error: Can't assign to the const variable 'pi'.
  print(exampleWithFinal(1));
  print(exampleWithFinal(2));
  print(exampleWithFinal(4));
}

double exampleWithFinal(final int x) {
  // x = x * 2; // Error: Can't assign to the final variable 'x'.
  const pi = 3.14;
  return x * pi;
}

/*
  Operators (+, -, *, /, %)
  % = reminder - what remains from a division operation.
  dividend / divisor = quotient R reminder
 */
void mathematicalOperatorsExample() {
  print('Operators - Mathematical operators example:');

  int n = 34, m = 2;
  const pi = 3.14;
  var gravity = 9.8;
  print('$n + $m = ${n + m}');
  print('$n - $m = ${n - m}');
  print('$m - $n = ${m - n}');
  print('$n * $m = ${n * m}');
  print('$n / $m = ${n / m}');
  print('$m / $n = ${m / n}');

  print('$gravity + $pi = ${gravity + pi}');
  print('$gravity - $pi = ${gravity - pi}');
  print('$pi - $gravity = ${pi - gravity}');
  print('$gravity * $pi = ${gravity * pi}');
  print('$gravity / $pi = ${gravity / pi}');
  print('$pi / $gravity = ${pi / gravity}');

  print('4 / 1 = ${4 / 1}');
  print('4 (dividend) / 1 = (divisor) = ${_getQuotient(4, 1)} (quotient) R ${_getReminder(4, 1)} (reminder)');

  print('4 / 2 = ${4 / 2}');
  print('4 (dividend) / 2 = (divisor) = ${_getQuotient(4, 2)} (quotient) R ${_getReminder(4, 2)} (reminder)');

  print('4 / 3 = ${4 / 3}');
  print('4 (dividend) / 3 = (divisor) = ${_getQuotient(4, 3)} (quotient) R ${_getReminder(4, 3)} (reminder)');

  print('4 / 4 = ${4 / 4}');
  print('4 (dividend) / 4 = (divisor) = ${_getQuotient(4, 4)} (quotient) R ${_getReminder(4, 4)} (reminder)');

  print('5 / 1 = ${5 / 1}');
  print('5 (dividend) / 1 = (divisor) = ${_getQuotient(5, 1)} (quotient) R ${_getReminder(5, 1)} (reminder)');

  print('5 / 2 = ${5 / 2}');
  print('5 (dividend) / 2 = (divisor) = ${_getQuotient(5, 2)} (quotient) R ${_getReminder(5, 2)} (reminder)');

  print('5 / 3 = ${5 / 3}');
  print('5 (dividend) / 3 = (divisor) = ${_getQuotient(5, 3)} (quotient) R ${_getReminder(5, 3)} (reminder)');

  print('5 / 4 = ${5 / 4}');
  print('5 (dividend) / 4 = (divisor) = ${_getQuotient(5, 4)} (quotient) R ${_getReminder(5, 4)} (reminder)');

  print('5 / 5 = ${5 / 5}');
  print('5 (dividend) / 4 = (divisor) = ${_getQuotient(5, 4)} (quotient) R ${_getReminder(5, 5)} (reminder)');
}

int _getQuotient(int n, int m) {
  return (n ~/ m); // a less efficient way is (n / m).toInt();
}

int _getReminder(int n, int m) {
  return n % m;
}

/*
  Equality and relational operators ( ==, !=, >, <, >=, <= )
 */
void equalityAndRelationalOperatorsExample() {
  print('Operators - Equality and relational operators example:');

  print('4 == 3 ? ${4 == 3}');
  print('4 != 3 ? ${4 != 3}');
  print('4 < 3 ? ${4 < 3}');
  print('4 <= 3 ? ${4 <= 3}');
  print('4 > 3 ? ${4 > 3}');
  print('4 >= 3 ? ${4 >= 3}');

  print('true == false ? ${true == false}');
  print('true != false ? ${true != false}');
  // print('true < false ? ${true < false}'); // Error: The operator '<' isn't defined for the class 'bool'.
  print('3 < 3.0001 ? ${3 < 3.0001}');
}

/*
  Type relational operators (as, is, is!)
 */
void typeRelationalOperatorsExample() {
  print('Operators - Type relational operators example:');

  const pi = 3.14;
  print('$pi is int = ${pi is int}');
  print('$pi is! int = ${pi is! int}');
  print('$pi is num = ${pi is num}');
  print('$pi is! num = ${pi is! num}');
  print('$pi is double = ${pi is double}');
  print('$pi is! double = ${pi is! double}');
}

/*
  Logical operators ( !, &&, || )
 */
void logicalOperatorsExample() {
  print('Operators - Logical operators example:');

  print('2 > 1 && 2 < 3 is ${2 > 1 && 2 < 3}');
  print('2 > 2 || 2 < 3 is ${2 > 2 || 2 < 3}');
  print('!(2 < 1) is ${!(2 < 1)}');
  print('false && true is ${false && true}');
  print('false || true is ${false || true}');
  print('!(false || true) is ${!(false || true)}');
  print('!true is ${!true}');
}

/*
  Logical operators ( !, &&, || )

  See: https://dart.dev/null-safety/understanding-null-safety
 */
void nullSafetyExample() {
  print('Types - NUll safety example:');

  // stdout.write('Enter your name : ');
  // String? str1 = stdin.readLineSync();
  // str1.isEmpty ? stderr.write('Name is empty!!') : stdout.write('Good Morning $str1');
  // Error: Property 'isEmpty' cannot be accessed on 'String?' because it is potentially null.
  // Try accessing using ?. instead.

  String s;
  // print('IsEmpty? ${s.isEmpty}'); // NoSuchMethodError: The getter 'isEmpty' was called on null.
  //print('IsEmpty? ${s?.isEmpty}'); // IsEmpty? null
}
