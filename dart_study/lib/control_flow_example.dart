/*
    Control flow
 */

void runControlFlowExample() {
  ifStatementExample(1, 2);
  ifStatementExample(2, 2);
  ifStatementExample(3, 2);

  forLoopsExample();
  whileAndDoLoopsExample();

  switchExample(16);
  switchExample(17);
  switchExample(18);
  switchExample(19);
  switchExample(20);

  functionExample();
  returningAnExpressionExample();
  optionalParametersExample();
  lexicalScopeAndInnerFunctionsExample();
}

/*
   If statement
 */

void ifStatementExample(int i, int j) {
  print('Control flow - If statement example:');

  if (i != j) {
    print('$i and $j are not the same');
  } else {
    print('$i and $j are the same');
  }

  if (i < j) {
    print('$i < $j is true');
  } else if (i == j) {
    print('$i == $j is true');
  } else {
    print('$i > $j is true');
  }
}

void forLoopsExample() {
  print('Control flow - For loop example:');
  for (var i = 0; i < 10; i++) {
    if (i % 2 == 0) {
      print('i = $i');
    }
  }
}

void whileAndDoLoopsExample() {
  print('Control flow - While and do loop example:');
  var i = 0;

  while (i < 10) {
    print('whjle loop, i = $i');
    i++;
  }

  do {
    print('do loop, i = $i');
  } while (i < 10);

  while (i < 20) {
    if (i == 11) {
      print('i == $i, exit.');
      break;
    }
    i++;
  }
}

void switchExample(int age) {
  print('Control flow - Switch example:');
  switch (age) {
    case 18:
    case 19:
      print('Age $age -> in the army now');
      break;
    default:
      print('Age $age -> civil life');
  }
}

void functionExample() {
  print('Control flow - Function example:');

  int i;
  i = countContains(['dart', 'csharp', 'dart again', 'java'], 'dart');
  print("Found 'dart' $i times");
}

int countContains(List<String> texts, String phrase) {
  var res = 0;
  for (var i = 0; i < texts.length; i++) {
    if (texts[i].contains(phrase)) {
      res++;
    }
  }
  return res;
}

/*
  Return an expression with =>
 */
void returningAnExpressionExample() {
  print('Control flow - Returning an expression example:');

  print('Full name: ${_fullName('James', 'Bond')}');
}

String _fullName(fName, lName) => '$fName $lName';

/*
  Optional parameters
 */
void optionalParametersExample() {
  print('Control flow - Optional parameters example:');
  print(_sayHello('Atanas', 'Hristov'));
  print(_sayHello('Atanas', 'Hristov', 51));

  print(_carInfo('Opel'));
  print(_carInfo('Opel', 'Zafira'));
  print(_carInfo('Opel', 'Zafira', 2018));
}

String _sayHello(String fName, String lName, [int? age]) =>
    age == null ? 'Hello, $fName $lName!' : 'Hello, $fName $lName, $age years young!';

String _carInfo(String carMake, [String? carModel, int carYear = 2020]) =>
    carModel == null ? 'Car information: $carMake ($carYear)' : 'Car information: $carMake $carModel ($carYear)';

/*
  Lexical scope
 */

void lexicalScopeAndInnerFunctionsExample() {
  print('Control flow - Lexical scope and inner functions example:');

  String counterTitle() => 'Counter is $_counter';

  print(counterTitle());
  _counter++;

  print(counterTitle());
  _counter++;

  print(counterTitle());
  _counter++;
}

var _counter = 0;
