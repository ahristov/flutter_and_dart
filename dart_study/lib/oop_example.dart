void runOOPExample() {
  var mic = Microphone('Bose', 'red', 12);
  print('mic ${mic.name} with color ${mic.color} and model ${mic.model}');
  mic.turnOn();
  mic.setVolume();
  mic.turnOff();

  mic = Microphone.initialize();
  print('mic ${mic.name} with color ${mic.color} and model ${mic.model}');

  mic = Microphone.instantiateWithName('Tesla');
  print('mic ${mic.name} with color ${mic.color} and model ${mic.model}');

  mic.setName = 'Videotone';
  print('mic ${mic.getName} with color ${mic.color} and model ${mic.model}');
}

class Microphone {
  // Instance variables
  String? name;
  String? color;
  int model = 345;

  // Default constructor - only one can exist

  /*
  Microphone(); // this is created by default
  */

  /*
  Microphone(String name) { // using the Java way
    this.name = name;
  }
  */

  /*
  Microphone(this.name, this.color, this.model) { // Dart way
    color = 'blue';
  }
  */

  Microphone(this.name, this.color, this.model); // Dart way

  // Named constructors

  Microphone.initialize() {
    name = 'OEM';
    color = 'gray';
  }

  Microphone.instantiateWithName(this.name);

  // Getters and setters
  // Can access directly the public variables instead.

  String? get getName => name;
  set setName(String value) => name = value;

  // Methods
  void turnOn() {
    print('mic $name with color $color is on');
  }

  void turnOff() {
    print('mic $name with color $color is turned off');
  }

  void setVolume() {
    print('mic $name with color $color volume is up');
  }

  bool isOn() => true;
  int modelNumber() => model;
}
