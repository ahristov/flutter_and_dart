import 'dart:io';

/*
    User input
 */

void runUserInputExample() {
  inputExample();
}

void inputExample() {
  stdout.writeln('What is your name?');
  var name = stdin.readLineSync();

  (name ?? '').isEmpty ? stderr.writeln('Name is empty!') : print('Hello ${name}!');
}
