import 'dart:async';

import 'package:dart_intro/oop_mixins_example.dart';
import 'package:get_it/get_it.dart';
// import 'package:dart_intro/bloc_example.dart';
// import 'package:dart_intro/collections_example.dart';
// import 'package:dart_intro/control_flow_example.dart';
// import 'package:dart_intro/oop_example.dart';
// import 'package:dart_intro/oop_inheritance_example.dart';
// import 'package:dart_intro/oop_interfaces_example.dart';
// import 'package:dart_intro/user_input_example.dart';
// import 'package:dart_intro/variables_example.dart';
// import 'package:get_it/get_it.dart';

// Prints:
// Instantiating Startup2...
// Startup2 does something...
// Startup2 does something...
// Startup2 does something...
// Disposing Startup2...
void main(List<String> arguments) {
  // runVariablesExample();
  // runControlFlowExample();
  // runOOPExample();
  // runOOPInheritanceExample();
  // runOOPInterfacesExample();
  // runUserInputExample();
  // runCollectionsExample();
  // runStreamsExample();
  // runBlocExample();

  runMixinsExample();

  // testStartup();
  // testStartup();
  // testStartup();
  // GetIt.I.reset(); // We have to explicitly call `reset()`, so that GetIt will invoke `onDispose()` on the singleton.

  const webviewURL =
      'https://epayments-epayui-uat-3.money-movement.com/PP_JW21611_PC/RemoteAccess?brId=9055&keytkn=yHSUEWAtxiMtu8Tu77dUTQ==Wwttn2i4gLiAizORsKF7BVdTXFP5Pc2+PC/Pqf94x6AdU/M16hw4MdYjz2k3F+xtX/qxu2oel1cdw8hxbnS6GYVIy0thvk2tndNHJBhF8+5LQ23QgXYGom5V6j9PFkyYT0y5JTJAVBJG9kAkxd/QbL3SUaxJr4VP9hEiSMwiIGk=&authtkn=dVUBgZKYuoFaKiD+xAj4h+Qo6ZMnkZKaIK82J1LGPicIivfoI3cT9NpH4F8fDfE01xsDTPF2v6ogzUZWj/SbuFXEy6ZP9F0dx0CRQLU+hY2LX58/ZlOniBpZhXHDZ6aBLbkhUQamlOZkxL7dEjvxiStQpfLMExfKxPtB7OJhY6zfE5fOPREK1XuXD0hph7qWG7NAG5kdqPu/UXxpfBJDd8hjZXe0/siOemUvUj+5tCJ4RKunC3ACpCEIBwiY77AQ7bZY2NzfEin/GaQ3BrYqw5XnNQhUYK/nXgSj0emCrcHz4CLiqd/UzxxQDgXc9fu8SDd/VsWHMQgT6jsrOG/CQqAf1zngSsT3FDkxN9ekLbgl8nT8UDcmK5dw9o7gTvg4c+QGGuy/2vV25NxpfI7gkTfJjd+qCWNJga5ZOYJzOm1NF86GB3+DhNl7LSwuBqxgRqSPLC1FCCCrZps3UC5rFRiB6amGr72Z15bfOXlZFYQtb+14DMV1yfoIu7jxrjRZalF1k5NE7VAeytnb71NCMJ6bQTOZQsJRvWXNd1T3IvvOxcr/IQRP3ZVZ96rb/oiFcS+S6n9tOTyqQcbtK/ek9KcokfRLA0sAgJqd85TtYoFGR8yb9qWswtwzmxMdHQqGSKSrUG1eDcpINh6FxMq/ke5Ge8Y2Gyzaa0K/NPnUfmurFENdnCF1/ZgvdNheJjvPZF+qP3mHZ0aLHAni2sx6Hw==&msgUUID=443980e1-73b7-4a58-80dd-c30f2d577c1d&memId=15544363';
  const expectedPostUrl = 'https://epayments-epayui-uat-3.money-movement.com/PP_JW21611_PC/RemoteAccess';
  const expectedPostData =
      'brId=9055&keytkn=yHSUEWAtxiMtu8Tu77dUTQ%253D%253DWwttn2i4gLiAizORsKF7BVdTXFP5Pc2%252BPC%252FPqf94x6AdU%252FM16hw4MdYjz2k3F%252BxtX%252Fqxu2oel1cdw8hxbnS6GYVIy0thvk2tndNHJBhF8%252B5LQ23QgXYGom5V6j9PFkyYT0y5JTJAVBJG9kAkxd%252FQbL3SUaxJr4VP9hEiSMwiIGk%253D&authtkn=dVUBgZKYuoFaKiD%252BxAj4h%252BQo6ZMnkZKaIK82J1LGPicIivfoI3cT9NpH4F8fDfE01xsDTPF2v6ogzUZWj%252FSbuFXEy6ZP9F0dx0CRQLU%252BhY2LX58%252FZlOniBpZhXHDZ6aBLbkhUQamlOZkxL7dEjvxiStQpfLMExfKxPtB7OJhY6zfE5fOPREK1XuXD0hph7qWG7NAG5kdqPu%252FUXxpfBJDd8hjZXe0%252FsiOemUvUj%252B5tCJ4RKunC3ACpCEIBwiY77AQ7bZY2NzfEin%252FGaQ3BrYqw5XnNQhUYK%252FnXgSj0emCrcHz4CLiqd%252FUzxxQDgXc9fu8SDd%252FVsWHMQgT6jsrOG%252FCQqAf1zngSsT3FDkxN9ekLbgl8nT8UDcmK5dw9o7gTvg4c%252BQGGuy%252F2vV25NxpfI7gkTfJjd%252BqCWNJga5ZOYJzOm1NF86GB3%252BDhNl7LSwuBqxgRqSPLC1FCCCrZps3UC5rFRiB6amGr72Z15bfOXlZFYQtb%252B14DMV1yfoIu7jxrjRZalF1k5NE7VAeytnb71NCMJ6bQTOZQsJRvWXNd1T3IvvOxcr%252FIQRP3ZVZ96rb%252FoiFcS%252BS6n9tOTyqQcbtK%252Fek9KcokfRLA0sAgJqd85TtYoFGR8yb9qWswtwzmxMdHQqGSKSrUG1eDcpINh6FxMq%252Fke5Ge8Y2Gyzaa0K%252FNPnUfmurFENdnCF1%252FZgvdNheJjvPZF%252BqP3mHZ0aLHAni2sx6Hw%253D%253D&msgUUID=443980e1-73b7-4a58-80dd-c30f2d577c1d&memId=15544363';

  final postData = _getPostDataFromWebViewUrl(webviewURL);
  print('postData: $postData');
}

String _getPostDataFromWebViewUrl(String webViewUrl) {
  var postData = '';
  final uri = Uri.tryParse(webViewUrl);
  if (uri?.hasQuery == false) {
    return postData;
  }

  for (final parm in uri!.queryParameters.keys) {
    print(parm);
  }
  return postData;
}

void testStartup() {
  if (!GetIt.I.isRegistered<Startup>()) {
    GetIt.I.registerLazySingleton<Startup>(() => Startup());
  }

  GetIt.I<Startup>().doSomething();
  StartupSingleton().doSomething();
}

class Startup implements Disposable {
  Startup() {
    print('Instantiating Startup2...');
  }

  void doSomething() {
    print('Startup2 does something...');
  }

  // This function is called when you pop or reset the scope or when you reset GetIt completely.
  @override
  FutureOr onDispose() {
    print('Disposing Startup2...');
  }
}

class StartupSingleton implements Disposable {
  static StartupSingleton? _instance;

  factory StartupSingleton() {
    return _instance ??= StartupSingleton._();
  }

  StartupSingleton._();

  void doSomething() {
    print('Startup does something');
  }

  @override
  FutureOr onDispose() {
    print('Disposing Startup2...');
  }
}
