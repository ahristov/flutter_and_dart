import 'package:flutter/material.dart';

void main() {
  runApp(ContentUIApp());
}

class ContentUIApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Content UI',
      theme: ThemeData(),
      home: Scaffold(
        body: ContentUIInterstitial(
          model: ContentModelInterstitial(
            headline: 'Cupidatat fugiat exercitation',
            overline:
                'Proident qui culpa enim nisi eu duis ipsum cillum id nisi nisi. Ad magna aliquip commodo elit...',
            description: ContentElementDescription(
                type: 'formattedText',
                format: 'markdown',
                text:
                    '''Cupidatat fugiat exercitation consectetur nisi minim ea officia dolor. Reprehenderit eu excepteur eiusmod esse voluptate eu minim. Duis sit nostrud cupidatat anim eiusmod amet amet nisi in. Reprehenderit sint voluptate amet in dolore ea. Lorem mollit magna ipsum labore et commodo Lorem deserunt dolor eiusmod proident. Cillum cupidatat excepteur proident esse laborum ea.

Proident qui culpa enim nisi eu duis ipsum cillum id nisi nisi. Ad magna aliquip commodo elit mollit fugiat proident labore pariatur ex elit. Exercitation occaecat irure id dolore excepteur tempor veniam laborum duis excepteur. Eu cillum reprehenderit sunt exercitation est.

Quis qui ipsum velit proident anim occaecat laborum tempor nulla sint proident dolor reprehenderit sit. Occaecat cillum aliquip consectetur ipsum mollit consectetur tempor eiusmod et dolor. Est consectetur esse commodo consequat velit in velit excepteur dolore sit eiusmod. Eu sint cillum mollit anim nostrud officia.

Anim cupidatat proident sunt sunt adipisicing labore aliqua. Sunt dolore nulla duis aliquip do anim enim incididunt. Velit labore est proident commodo Lorem.

Sunt minim aliqua exercitation qui magna dolor nostrud sit. Non eu commodo Lorem eu dolore ipsum nulla qui. Reprehenderit fugiat quis laborum nisi pariatur do adipisicing veniam sint sint qui. Anim voluptate nisi velit veniam labore mollit officia ex ullamco reprehenderit commodo quis cillum. Enim tempor laborum qui minim et magna velit in officia ipsum consectetur. Ad sint incididunt aliqua irure exercitation aliqua commodo aute pariatur tempor.

Incididunt elit duis consequat nostrud enim adipisicing nulla id quis aliquip ipsum. Nulla amet quis tempor fugiat. In fugiat exercitation aliqua et aliquip.

Ea cupidatat consequat ipsum nulla mollit cillum consequat tempor nulla quis nostrud ad sunt Lorem. Laborum aute irure Lorem cupidatat aliqua. Aliquip ea velit labore commodo ipsum. Consectetur anim duis elit pariatur adipisicing consequat nostrud fugiat culpa commodo laborum eiusmod cupidatat duis. Amet Lorem ullamco sint labore qui.

Nostrud mollit velit voluptate consectetur ipsum nulla fugiat duis. Amet ipsum id anim commodo fugiat ipsum fugiat magna adipisicing anim. Consectetur consectetur aliquip aute duis. Ea elit tempor labore sunt irure ipsum consectetur velit tempor in cupidatat. Ipsum consequat irure irure occaecat commodo proident nostrud magna officia Lorem cillum. Magna cupidatat exercitation dolor esse consequat officia culpa sit sunt ad consequat nisi nulla veniam. Ex nostrud dolor occaecat anim ut consequat occaecat Lorem laborum veniam consectetur cillum.

Sit pariatur veniam culpa incididunt enim enim nulla. Consequat est eu ipsum aute eu eu nisi id ut enim in pariatur labore. Labore aliqua quis voluptate do magna fugiat eu ad reprehenderit aute tempor esse. Aute exercitation tempor id id aute veniam non sint velit aliquip non nulla dolor. Commodo sunt sit laborum laborum consequat proident tempor consequat labore commodo fugiat. Mollit reprehenderit et cillum irure mollit proident ex cupidatat nisi in non aliquip.

Aliqua anim dolore aute excepteur duis Lorem. Do elit irure eiusmod dolore. Amet cupidatat duis do et magna ex irure. Exercitation officia eu nulla excepteur sunt irure cillum dolore tempor do. Culpa labore sunt eiusmod aute nulla qui voluptate sint mollit ipsum incididunt ea aliqua.
            '''),
            buttons: [
              ContentElementButton(
                type: 'button',
                text: 'Not Now',
                style: 'secondary',
                interaction: ContentElementInteraction(
                  type: 'interaction',
                  kind: 'delay',
                  delayType: 'days',
                  delayBy: 1,
                ),
                destination: null,
              ),
              ContentElementButton(
                type: 'button',
                text: 'Not interested',
                style: 'ghost',
                interaction: ContentElementInteraction(
                  type: 'interaction',
                  kind: 'delay',
                  delayType: 'days',
                  delayBy: 1,
                ),
                destination: null,
              ),
              ContentElementButton(
                type: 'button',
                text: 'Learn Now',
                style: 'primary',
                interaction: ContentElementInteraction(
                  type: 'interaction',
                  kind: 'accept',
                  delayType: null,
                  delayBy: null,
                ),
                destination: null,
              ),
            ],
          ),
          image: Image.asset('images/yoda.png'),
        ),
      ),
    );
  }
}

class ContentUIInterstitial extends StatelessWidget {
  final ContentModelInterstitial model;
  final Image? image;

  const ContentUIInterstitial({required this.model, required this.image});

  @override
  Widget build(BuildContext context) {
    final sliverWidgets = [
      if (image != null) ...[
        ClipRect(
          child: Align(
            widthFactor: 1.0,
            heightFactor: 0.8,
            child: image,
          ),
        ),
        SizedBox(
          height: 16,
        ),
      ],
      if (model.overline != null) ...[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            model.overline!.toUpperCase(),
            style: TextStyle(fontSize: 16),
          ),
        ),
        SizedBox(
          height: 16,
        ),
      ],
      if (model.headline != null) ...[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            model.headline!,
            style: TextStyle(fontSize: 24),
          ),
        ),
        SizedBox(
          height: 16,
        ),
      ],
      if (model.description != null && model.description!.text != null && model.description!.text!.isNotEmpty)
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Text(
            model.description!.text!,
          ),
        ),
    ];

    final List<Widget> buttons = [];
    if (model.buttons != null) {
      [...model.buttons!].sort();
      buttons.addAll(model.buttons!.map((button) {
        final buttonColor = const {
              ContentButtonStyleType.primary: Colors.blue,
              ContentButtonStyleType.secondary: Colors.green,
            }[button.buttonStyleType] ??
            Colors.red;
        return RoundedButton(
          color: buttonColor,
          title: button.text ?? '',
          onPressed: () {
            print('${button.text ?? ''} was pressed');
          },
        );
      }));
    }

    return Material(
      child: Stack(
        //fit: StackFit.expand,
        children: [
          Container(
            // constraints: const BoxConstraints.expand(),
            child: Column(children: [
              Expanded(
                child: CustomScrollView(
                  slivers: [
                    SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) => sliverWidgets[index],
                        childCount: sliverWidgets.length,
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                children: [
                  ...buttons,
                  SizedBox(
                    height: 16,
                  ),
                ],
              ),
              // SafeArea(
              //   child: Column(
              //     children: buttons,
              //   ),
              // ),
            ]),
          ),
          Positioned(
            left: 10.0,
            top: 60.0,
            child: MaterialButton(
              onPressed: () {},
              color: Color.fromRGBO(250, 250, 250, 0.3),
              textColor: Colors.white,
              child: Icon(
                Icons.expand_more_rounded,
                size: 24,
              ),
              padding: EdgeInsets.all(8),
              shape: CircleBorder(),
            ),
          ),
        ],
      ),
    );
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class ContentModelInterstitial {
  final String? headline;
  final String? overline;
  final ContentElementDescription? description;
  final List<ContentElementButton>? buttons;

  ContentModelInterstitial({
    this.headline,
    this.overline,
    this.description,
    this.buttons,
  });
}

class ContentElementDescription {
  final String? format;
  final String? text;
  final String? type;

  static const String _kFormattedTextType = 'formattedText';
  static const String _kMarkdownFormat = 'markdown';

  ContentElementDescription({
    required this.type,
    required this.format,
    required this.text,
  });

  @override
  String toString() => 'ContentElementDescription(type: $type, format: $format, text: $text)';

  bool get isMarkdownText => type == _kFormattedTextType && format == _kMarkdownFormat;
}

class ContentElementButton implements Comparable<ContentElementButton> {
  final String? type;
  final String? text;
  final String? style;
  final ContentElementInteraction? interaction;
  final ContentElementDestination? destination;

  ContentElementButton({
    required this.type,
    required this.text,
    required this.style,
    required this.interaction,
    required this.destination,
  });

  ContentButtonStyleType get buttonStyleType => style.toContentButtonStyleType;

  @override
  int compareTo(ContentElementButton other) {
    final ordThis = const {
          ContentButtonStyleType.primary: 1,
          ContentButtonStyleType.secondary: 2,
        }[buttonStyleType] ??
        3;
    final ordOther = const {
          ContentButtonStyleType.primary: 1,
          ContentButtonStyleType.secondary: 2,
        }[other.buttonStyleType] ??
        3;
    return ordThis.compareTo(ordOther);
  }

  @override
  String toString() {
    return 'ContentElementButton(type: $type, text: $text, style: $style, interaction: $interaction, destination: $destination)';
  }
}

const String kPrimaryButtonStyle = 'primary';
const String kSecondaryButtonStyle = 'secondary';
const String kGhostButtonStyle = 'ghost';

enum ContentButtonStyleType {
  primary,
  secondary,
  ghost,
}

extension ContentButtonStyleTypeExtension on String? {
  ContentButtonStyleType get toContentButtonStyleType {
    switch (this) {
      case kPrimaryButtonStyle:
        return ContentButtonStyleType.primary;
      case kSecondaryButtonStyle:
        return ContentButtonStyleType.secondary;
      default:
        return ContentButtonStyleType.ghost;
    }
  }
}

class ContentElementInteraction {
  final String? type;
  final String? kind;
  final String? delayType; // "minutes"
  final int? delayBy;

  ContentElementInteraction({
    required this.type,
    required this.kind,
    required this.delayType,
    required this.delayBy,
  });

  @override
  String toString() {
    return 'ContentElementInteraction(type: $type, kind: $kind, delayType: $delayType, delayBy: $delayBy)';
  }
}

class ContentElementDestination {
  final String? type;
  final String? kind;
  final String? widget;
  final String? subpath;
  final String? target;
  final String? uri;

  ContentElementDestination({
    required this.type,
    required this.kind,
    required this.widget,
    required this.subpath,
    required this.target,
    required this.uri,
  });

  @override
  String toString() {
    return 'ContentElementDestination(type: $type, kind: $kind, widget: $widget, subpath: $subpath, target: $target, uri: $uri)';
  }
}

class RoundedButton extends StatelessWidget {
  RoundedButton({
    required this.color,
    required this.title,
    required this.onPressed,
  });

  final Color color;
  final String title;
  final Function() onPressed;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: Material(
        elevation: 5.0,
        color: color,
        borderRadius: BorderRadius.circular(30.0),
        child: MaterialButton(
          onPressed: onPressed,
          minWidth: 300.0,
          height: 30.0,
          child: Text(
            title,
            style: TextStyle(color: Colors.white),
          ),
        ),
      ),
    );
  }
}
