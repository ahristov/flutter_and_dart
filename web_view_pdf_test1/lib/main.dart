import 'dart:developer';
import 'dart:io';
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

void main() {
  runApp(const WebViewExample());
}

class WebViewExample extends StatefulWidget {
  const WebViewExample({super.key});

  @override
  State<WebViewExample> createState() => _WebViewExampleState();
}

class _WebViewExampleState extends State<WebViewExample> {
  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = AndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(),
        body: WebView(
          javascriptMode: JavascriptMode.unrestricted,
          zoomEnabled: true,
          // initialUrl: 'https://www.postgresql.org/docs/',
          // initialUrl:
          //     'https://drive.google.com/viewerng/viewer?embedded=true&url=https://www.postgresql.org/files/documentation/pdf/15/postgresql-15-US.pdf',
          initialUrl: 'https://www.postgresql.org/files/documentation/pdf/15/postgresql-15-US.pdf',
          onPageStarted: onPageStarted,
          onPageFinished: onPageFinished,
          onWebResourceError: onWebResourceError,
          navigationDelegate: navigationDelegate,
        ),
      ),
    );
  }

  void onPageStarted(String url) {
    log('onPageStarted: $url');
  }

  void onPageFinished(String url) {
    log('onPageFinished: $url');
  }

  Future<void> onWebResourceError(WebResourceError error) async {
    log('onWebResourceError ${error.description}');
  }

  FutureOr<NavigationDecision> navigationDelegate(NavigationRequest request) async {
    log('navigationDelegate ${request.url}');
    return NavigationDecision.navigate;
  }
}
