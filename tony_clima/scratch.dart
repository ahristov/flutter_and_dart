import 'dart:core';

void main() {
  performTasks();
}

void performTasks() async {
  taskOne();
  String task2Result = await taskTwo();
  taskThree(task2Result);
}

void taskOne() {
  print('Task 1 complete');
}

Future taskTwo() async {
  Duration duration = Duration(seconds: 1);
  String result = '';
  await Future.delayed(duration, () {
    result = 'task 2 data';
    print('Task 2 complete');
  });
  return result;
}

void taskThree(String task2Data) {
  print('Task 3 complete with $task2Data');
}
