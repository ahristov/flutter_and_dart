import 'package:geolocator/geolocator.dart';

class Location {
  double latitude = 0;
  double longitude = 0;

  Future<void> getCurrentLocation() async {
    try {
      // In order this to work on Android emulator
      // - Add to AndroidManifest.xml
      //     <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
      //     <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
      // - Set the device location from the '...' on the emulator menus or via the console:
      //     adb emu geo fix -96.82 33.15
      // - Stop and re-run the app

      Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.low,
        forceAndroidLocationManager: true,
        timeLimit: Duration(seconds: 5),
      );
      latitude = position.latitude;
      longitude = position.longitude;
    } catch (e) {
      print(e);
    }
  }
}
