import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        // appBar: AppBar(backgroundColor: Colors.red, title: Text('Demo')),
        backgroundColor: Colors.teal,
        body: SafeArea(
          child: Row(
            // Normally it takes the maximum available size
            // We can change the `mainAxisSize` to `MainAxisSize.min`
            // to only fit it's children.
            // mainAxisSize: MainAxisSize.min,

            // If the container takes all the space (which is by default),
            // then we set can `mainAxisAlignment` to `MainAxisAlignment.center`
            // to center the content.
            // Or we can use `MainAxisAlignment.spaceEvenly` to space evenly.
            // Or we can use `MainAxisAlignment.spaceBetween`
            // to start from beginning and end at the end of the Column/Row.
            mainAxisAlignment: MainAxisAlignment.spaceBetween,

            // Alignment across (perpendicular) to the main axis.
            // It aligns to the children's size.
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Container(
                height: 100.0,
                width: 70.0,
                color: Colors.red,
              ),
              Container(
                height: 100.0,
                width: double.infinity,
                color: Colors.teal,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      height: 70.0,
                      width: 70.0,
                      color: Colors.yellow,
                    ),
                    Container(
                      height: 70.0,
                      width: 70.0,
                      color: Colors.green,
                    ),
                  ],
                ),
              ),
              Container(
                height: 100.0,
                width: 70.0,
                color: Colors.blue,
              ),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.red,
          child: Icon(Icons.add),
        ),
      ),
    );
  }
}
