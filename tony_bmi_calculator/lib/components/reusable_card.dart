import 'package:flutter/material.dart';

class ReusableCard extends StatelessWidget {
  // The StatelessWidget class is marked as `@immutable`,
  // thereof the fields should be immutable too. We declare these as `final`.
  final Color color;
  final Widget? cardChild;
  final void Function()? onPress;

  ReusableCard({required this.color, this.cardChild, this.onPress});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        child: cardChild,
        margin: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: color,
        ),
      ),
    );
  }
}
