import 'package:flutter/material.dart';

class RoundIconButton extends StatelessWidget {
  final IconData icon;
  final void Function()? onPress;

  RoundIconButton({required this.icon, this.onPress});

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: onPress,
      child: Icon(icon),
      shape: CircleBorder(),
      fillColor: Color(0xff4c4f5e),
      elevation: 0.0,
      constraints: BoxConstraints.tightFor(
        width: 42.0,
        height: 42.0,
      ),
    );
  }
}
