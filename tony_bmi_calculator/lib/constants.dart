import 'dart:ui';

import 'package:flutter/material.dart';

const kActiveCardColor = Color(0xFF1d1e33);
const kInactiveCardColor = Color(0xFF111328);

const kLabelColor = Color(0xff8d8e98);
const kLabelTextStyle = TextStyle(
  fontSize: 18.0,
  color: kLabelColor,
);
const kNumberTextStyle = TextStyle(
  fontSize: 36.0,
  color: Colors.white,
  fontWeight: FontWeight.bold,
);

const kLargeButtonTextStyle = TextStyle(
  fontSize: 24.0,
  fontWeight: FontWeight.bold,
);

const kTitleTextStyle = TextStyle(
  fontSize: 34.0,
  fontWeight: FontWeight.bold,
);

const kResultsTextStyle = TextStyle(
  fontSize: 32.0,
  fontWeight: FontWeight.bold,
  color: Color(0xff24d876),
);

const kResultsNumberStyle = TextStyle(
  fontSize: 36.0,
  fontWeight: FontWeight.bold,
);

const kResultsBodyStyle = TextStyle(
  fontSize: 16.0,
  fontWeight: FontWeight.normal,
);

const kBottomContainerColor = Color(0xFFeb1555);
const kBottomContainerHeight = 80.0;
