import 'dart:collection';

import 'package:flutter/foundation.dart';

import 'task.dart';

class TaskData extends ChangeNotifier {
  List<Task> _tasks = [
    Task(name: 'Task one'),
    Task(name: 'Task two', isDone: true),
    Task(name: 'Task three'),
    Task(name: 'Task four'),
  ];

  UnmodifiableListView<Task> get tasks => UnmodifiableListView(_tasks);

  int get taskCount => _tasks.length;

  void addTask(String newTaskText) {
    _tasks.add(Task(name: newTaskText));
    notifyListeners();
  }

  void toggleTask(Task task) {
    task.toggleDone();
    notifyListeners();
  }

  void deleteTask(Task task) {
    _tasks = _tasks.where((element) => element != task).toList();
    notifyListeners();
  }
}
