import 'package:flutter/material.dart';

const kAddTaskButtonTextStyle = TextStyle(
  color: Colors.white,
  backgroundColor: Colors.lightBlueAccent,
  fontWeight: FontWeight.bold,
  fontSize: 18.0,
);
