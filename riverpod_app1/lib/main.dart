/// Riverpod tutorial from https://youtu.be/Zp7VKVhirmw
///

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

abstract class WebSocketClient {
  Stream<int> getCounterStream([int start]);
}

class FakeWebSocketClient implements WebSocketClient {
  @override
  Stream<int> getCounterStream([int start = 0]) async* {
    int i = start;
    while (true) {
      await Future.delayed(const Duration(milliseconds: 500));
      yield i++;
    }
  }
}

// This provides the web socket client, but we want to get to the stream of integers.
final wsClientProvider = Provider<WebSocketClient>((ref) => FakeWebSocketClient());

// This way we are not passing in any initial data:
// final wsCounterProvider = StreamProvider.autoDispose<int>((ref) {
//   final wsClient = ref.watch(wsClientProvider);
//   return wsClient.getCounterStream();
// });

// `family<int, int>`: Allows to pass in initial data.
final wsCounterProvider = StreamProvider.autoDispose.family<int, int>((ref, startValue) {
  final wsClient = ref.watch(wsClientProvider);
  return wsClient.getCounterStream(startValue);
});

// `StateProvider()`: Create state provider which value can be used from outside.
// Use `StateProvider.autoDispose()` to create provider that will dispose
// (not keep value), once the widget disposed.
//
final counterProvider = StateProvider((ref) => 0);

void main() {
  runApp(
    const ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Counter App',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.green,
          brightness: Brightness.dark,
          surface: const Color(0xff003909),
        ),
      ),
      home: const HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: Center(
        child: ElevatedButton(
          child: const Text('Go to Counter Page'),
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: ((context) => const CounterPage()),
              ),
            );
          },
        ),
      ),
    );
  }
}

class CounterPage extends ConsumerWidget {
  const CounterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    // `ref.watch()`: Returns value from the provider.
    // It rebuilds the widget when the value changes.
    final int counter = ref.watch(counterProvider);

    // We can get the stream from the stream provider.
    // final Stream<int> wsCounterStream = ref.watch(wsCounterProvider.stream);

    // Or we can get an AsyncValue that wraps the int.
    // We use this instead of streams that we handle with StreamBuilder.
    // The widget still will be rebuild when we receive new counter value.
    final AsyncValue<int> wsCounter = ref.watch(wsCounterProvider(1000));

    // `ref.listen()`: Listen to state changes.
    // Use to perform navigation, alerts, snackbar
    // or any logic that has to happen per state change inside `build()`,
    // as otherwise we cannot run logic that "draws" when `build()` happens.
    ref.listen<int>(
      counterProvider,
      (previous, next) {
        if (next >= 5) {
          showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: const Text('Warning'),
                content: const Text('Counter very high. Consider reset it.'),
                actions: [
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Text('OK'),
                  ),
                ],
              );
            },
          );
        }
      },
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(
          wsCounter.when(
            data: (int value) => 'Counter $value',
            error: (Object e, _) => 'Error $e',
            loading: () => '...',
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {
              // `ref.refresh()`: Reevaluate the state and return the created value.
              // This is less optimal than "invalidate".
              // final x = ref.refresh(counterProvider);

              // `ref.invalidate()`: Immediately invalidates the state, causing it to refresh.
              // The provider will be disposed immediately.
              ref.invalidate(counterProvider);
            },
            icon: const Icon(Icons.refresh),
          )
        ],
      ),
      body: Center(
        child: Text(
          counter.toString(),
          style: Theme.of(context).textTheme.displayMedium,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          // `ref.read`: Reads a provider without listening to it.
          // Use it inside event handlers to modify the value.
          ref.read(counterProvider.notifier).state++;
        },
      ),
    );
  }
}
